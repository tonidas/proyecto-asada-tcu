<?php
/***************************************************
* Only these origins are allowed to upload images *
***************************************************/
$accepted_origins = array("http://unac.or.cr");
$url = "http://unac.or.cr/";

/*********************************************
* Change this line to set the upload folder *
*********************************************/
$imageFolder = "public/images/img_o/";
$imageFolderMD = "public/images/img_md/";
$imageFolderSM = "public/images/img_sm/";

function calcularDimensiones($originalWidth, $type) {
	$cambio = 1;
	//0 low 1 med
	$targetWidth = $type == 0 ? 380 : 730;
	for ($cambio = 1; $cambio > 0; $cambio = $cambio - 0.01) {
		$width = $originalWidth * $cambio;
		
		if ($width <= $targetWidth) {
			break;
		}
	}
	if ($cambio == 0) {
		$cambio = 1;
	}
	return $cambio;
}

function crearMiniatura($imagen, $oWidth, $oHeight, $type) {
	$cambio = calcularDimensiones($oWidth, $type);
	$width = $oWidth * $cambio;
	$height = $oHeight * $cambio;
	$thumb = imagecreatetruecolor($width, $height);
	imagecopyresampled(
		$thumb, 
		$imagen,
		0, 0, 0, 0,
		$width, 
		$height,
		$oWidth, 
		$oHeight
	);
	return $thumb;
}

if (isset($_SERVER['HTTP_ORIGIN'])) {

    if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);

        if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
          header('HTTP/1.1 200 OK');
          return;
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            reset ($_FILES);
            $temp = current($_FILES);
            if (is_uploaded_file($temp['tmp_name'])) {
                /*
                 If your script needs to receive cookies, set images_upload_credentials : true in
                 the configuration and enable the following two headers.
                */
                // header('Access-Control-Allow-Credentials: true');
                // header('P3P: CP="There is no P3P policy."');

                //Verificar que el nombre no contenga caracteres invalidos
                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                   header("HTTP/1.1 400 Invalid file name.");
                   return;
                }

                // Verify extension
                if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("jpg", "png"))) {
                   header("HTTP/1.1 400 Invalid extension.");
                   return;
                }
				//Verificar que existan las carpetas
				if (!is_dir($imageFolder)) {
					mkdir($imageFolder);
				}
				if (!is_dir($imageFolderMD)) {
					mkdir($imageFolderMD);
				}
				if (!is_dir($imageFolderSM)) {
					mkdir($imageFolderSM);
				}
                //Generador de nombres
                $length = 40;
                $extension = strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION));
				$keys = array_merge(range(0, 9), range('a', 'z'));
				
                do {
                     $key = '';

                     for ($i = 0; $i < $length; $i++) {
                         $key .= $keys[array_rand($keys)];
                     }
                } while (file_exists($imageFolder.'/'.$key.$extension));
				$nombre = $key;
				
				//Procesar la imagen dependiendo de su tipo
				if ($extension == 'png') {
					$imagen = imagecreatefrompng($temp['tmp_name']);
				}
				
				if ($extension == 'jpg') {
					$imagen = imagecreatefromjpeg($temp['tmp_name']);
				}
				//Verificar si es necesario recortar
				if (isset($_POST['crop'])) {
					$propsRecorte = json_decode($_POST['crop']);
					//Verificar que la area del crop exista
					if ($propsRecorte->widthC > 0 && $propsRecorte->heightC > 0) {			
						//Verificar que las dimensiones sean las correctas
						if ($propsRecorte->width / $propsRecorte->aspect == $propsRecorte->height) {				
							$width = imagesx($imagen);
							$height = imagesy($imagen);
							$escalaX = $width / $propsRecorte->widthC;
							$escalaY = $height / $propsRecorte->heightC;
							$recorte_width = $propsRecorte->width * $escalaX;
							$recorte_height = $propsRecorte->height * $escalaY;		
							$recorte = imagecreatetruecolor( $recorte_width, $recorte_height );
							//Recortar
							imagecopy(
							   $recorte, //Imagen final (recorte)
							   $imagen, //Imagen inicial
							   0, //Ubicacion en X del destino de la imagen
							   0, //Ubicacion en Y del destino de la imagen
							   $propsRecorte->x * $escalaX, //Ubicacion en X del recorte
							   $propsRecorte->y * $escalaY,	//Ubicacion en Y del recorte
							   $propsRecorte->width * $escalaX, //Anchura del recorte
							   $propsRecorte->height * $escalaY //Altura del recorte
							);
							//Medio
							$thumb = crearMiniatura($recorte, $propsRecorte->width * $escalaX, $propsRecorte->height * $escalaY, 1);
							imagejpeg($thumb, $imageFolderMD.$nombre.'.jpg', 100);
							//Pequeño
							$thumb = crearMiniatura($recorte, $propsRecorte->width * $escalaX, $propsRecorte->height * $escalaY, 0);
							imagejpeg($thumb, $imageFolderSM.$nombre.'.jpg', 100);
							//Guardar la imagen en jpg (recorte a guardar, ruta de destino, calidad(0-100))
							imagejpeg($recorte, $imageFolder.$nombre.'.jpg', 100);
							echo json_encode(array('location' => $url.$imageFolder.$nombre.'.jpg'));
							header('HTTP/1.1 200 OK');
							return;
						}
					}
				}
				$width = imagesx($imagen);
				$height = imagesy($imagen);
				//Medio
				$thumb = crearMiniatura($imagen, $width, $height, 1);
				imagejpeg($thumb, $imageFolderMD.$nombre.'.jpg', 100);
				//Pequeño
				$thumb = crearMiniatura($imagen, $width, $height, 0);
				imagejpeg($thumb, $imageFolderSM.$nombre.'.jpg', 100);
				//De no ocupar recorte, directamente se guarda como jpg
				imagejpeg($imagen, $imageFolder.$nombre.'.jpg', 100);
                echo json_encode(array('location' => $url.$imageFolder.$nombre.'.jpg'));
                header('HTTP/1.1 200 OK');
            } else {
                // Notify editor that the upload failed
                header("HTTP/1.1 500 Server Error");
            }
        } else {
            header("HTTP/1.1 403 Origin Denied");
            return;
        }
    }
}
?>
