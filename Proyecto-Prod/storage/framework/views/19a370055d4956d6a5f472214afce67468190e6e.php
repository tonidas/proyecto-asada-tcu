<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('libs/font-awesome/fontawesome-free-5.10.2-web/css/all.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('libs/bootstrap-4.3.1-dist/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/general.css')); ?>">

    <?php $__env->startSection('header_css'); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/header.css')); ?>">
    <?php echo $__env->yieldSection(); ?>
    <?php $__env->startSection('footer_css'); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/footer.css')); ?>">
    <?php echo $__env->yieldSection(); ?>

    <script type="text/javascript" src="<?php echo e(asset('libs/jquery/jquery-3.4.1.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('libs/popper/popper.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('libs/bootstrap-4.3.1-dist/js/bootstrap.min.js')); ?>"></script>

    <?php $__env->startSection('header_js'); ?>
        <script type="text/javascript" src="<?php echo e(asset('js/header.js')); ?>"></script>
    <?php echo $__env->yieldSection(); ?>
    <?php $__env->startSection('footer_js'); ?>
    <?php echo $__env->yieldSection(); ?>

    <?php echo $__env->yieldContent('links'); ?>
    <title><?php echo $__env->yieldContent('titulo'); ?></title>
  </head>
  <body>
    <?php $__env->startSection('header'); ?>
        <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldSection(); ?>

    <?php echo $__env->yieldContent('contenido'); ?>

    <?php $__env->startSection('footer'); ?>
        <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldSection(); ?>
  </body>
</html>

<?php echo $__env->yieldContent('botLinks'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/app.js')); ?>"></script>
<?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\layouts\base.blade.php ENDPATH**/ ?>