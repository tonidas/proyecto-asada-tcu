<?php $__env->startSection('links'); ?>
<link rel="stylesheet" href="<?php echo e(asset('css/donacion/donaciones_admin.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('titulo', 'Donaciones'); ?>

<?php $__env->startSection('Navbar'); ?>
<div id="header">
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contenido'); ?>
<div id="donacionesAdmin"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('botLinks'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('libs/DataTables/datatables.min.css')); ?> "/>
<script type="text/javascript" src="<?php echo e(asset('libs/DataTables/datatables.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\donacion\donaciones_admin.blade.php ENDPATH**/ ?>