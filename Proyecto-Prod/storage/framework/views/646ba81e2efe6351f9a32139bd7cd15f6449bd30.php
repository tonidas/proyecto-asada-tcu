<?php $__env->startSection('links'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/colaborador/editar_colaborador.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('titulo', 'Editar colaborador'); ?>

<?php $__env->startSection('header'); ?>
##parent-placeholder-594fd1615a341c77829e83ed988f137e1ba96231##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contenido'); ?>
<script>
const cedula = <?php echo e($cedula); ?>;
</script>
<div id="editarColaborador"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\colaborador\editar_colaborador.blade.php ENDPATH**/ ?>