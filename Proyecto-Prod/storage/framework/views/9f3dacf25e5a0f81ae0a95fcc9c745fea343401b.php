<?php $__env->startSection('links'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/noticia/noticia_usuario.css')); ?>">

<script type="text/javascript" src="<?php echo e(asset('js/noticia/noticia_usuario.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('titulo', '.'); ?>

<?php $__env->startSection('contenido'); ?>
    <script>
        const id = <?php echo e($id); ?>;
    </script>
    <div id="noticiaUsuario"></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\noticia\noticia_usuario.blade.php ENDPATH**/ ?>