<?php $__env->startSection('links'); ?>
<link rel="stylesheet" href="<?php echo e(asset('libs/typeahead/Typeahead.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('libs/cropper/css/cropper.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/noticia/crear_noticia.css')); ?>">

<script type="text/javascript" src="<?php echo e(asset('libs/cropper/js/cropper.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('libs/tinymce/tinymce.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('titulo', 'Crear donación'); ?>

<?php $__env->startSection('Navbar'); ?>
<div id="header">
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contenido'); ?>
<div id="crearDonaciones"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\donacion\crear_donacion.blade.php ENDPATH**/ ?>