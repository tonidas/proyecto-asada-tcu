<?php $__env->startSection('links'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/administrador/administradores_superadm.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('titulo', 'Administradores'); ?>

<?php $__env->startSection('header'); ?>
##parent-placeholder-594fd1615a341c77829e83ed988f137e1ba96231##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contenido'); ?>
<div id="administradoresSuperAdm"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('botLinks'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('libs/DataTables/datatables.min.css')); ?> "/>
<script type="text/javascript" src="<?php echo e(asset('libs/DataTables/datatables.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\administrador\administradores_superadm.blade.php ENDPATH**/ ?>