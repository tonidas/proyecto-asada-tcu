<?php $__env->startComponent('mail::layout'); ?>
    
    <?php $__env->slot('header'); ?>
        <?php $__env->startComponent('mail::header', ['url' => config('app.url')]); ?>
            Recuperación de contraseña
        <?php echo $__env->renderComponent(); ?>
    <?php $__env->endSlot(); ?>



<strong><?php echo e($data['nombre']); ?></strong>

Estás recibiendo este correo debido a que recibimos una solicitud para restablecer la contraseña de esta cuenta.

<?php $__env->startComponent('mail::button', ['url' => $data['url'], 'color' => 'blue']); ?>
Restablecer contraseña
<?php echo $__env->renderComponent(); ?>

Si no fuiste tú quien hizo la solicitud, ignora este mensaje.


    <?php if(isset($subcopy)): ?>
        <?php $__env->slot('subcopy'); ?>
            <?php $__env->startComponent('mail::subcopy'); ?>
                <?php echo e($subcopy); ?>

            <?php echo $__env->renderComponent(); ?>
        <?php $__env->endSlot(); ?>
    <?php endif; ?>


    <?php $__env->slot('footer'); ?>
        <?php $__env->startComponent('mail::footer'); ?>
            © <?php echo e(date('Y')); ?> <?php echo e(config('app.name')); ?>.
        <?php echo $__env->renderComponent(); ?>
    <?php $__env->endSlot(); ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\auth\passwords\diseño_email.blade.php ENDPATH**/ ?>