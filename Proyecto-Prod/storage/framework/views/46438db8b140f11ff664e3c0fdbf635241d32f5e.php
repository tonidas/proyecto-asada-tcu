<nav>
    <div class="nav-fostrap">
        <ul>
            <li class="title-web">
                <a href="/">ASADAS</a>
            </li>
            <?php if(Auth::check()): ?>
            <li><a href="javascript:void(0)">Administración<span class="arrow-down ml-2"></span></a>
                <ul class="dropdown">
                    <li><a href="/noticia">Noticias</a></li>
                    <li><a href="/colaborador">Colaboradores</a></li>
                    <?php if(Auth::user()->hasRole('Superusuario')): ?>
                    <li><a href="/administrador">Administradores</a></li>
                    <?php endif; ?>
                    <li><a href="/donacion">Donaciones</a></li>
                </ul>
            </li>
            <li><a href="javascript:void(0)">Navegación usuario<span class="arrow-down ml-2"></span></a>
                <ul class="dropdown">
                    <li><a href="/">Noticias</a></li>
                    <li><a href="/colaboradores">Colaboradores</a></li>
                    <li><a href="/donaciones">Donaciones</a></li>
                </ul>
            </li>
            <?php else: ?>
            <li>
                <a href="/">Noticias</a>
            </li>
            <li>
                <a href="/colaboradores">Colaboradores</a>
            </li>
            <li>
                <a href="/donaciones">Donaciones</a>
            </li>
            <?php endif; ?>
            <?php if(Auth::check()): ?>
            <li class="nav-right">
                <a href="<?php echo e(url('/logout')); ?>">Cerrar Sesión</a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="nav-bg-fostrap">
        <div class="navbar-fostrap"> <span></span> <span></span> <span></span> </div>
        <a href="/" class="title-mobile">ASADAS</a>
    </div>
</nav>
<div id="nav-back"></div>
<?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\layouts\header.blade.php ENDPATH**/ ?>