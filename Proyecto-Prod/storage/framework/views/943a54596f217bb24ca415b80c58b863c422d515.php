<footer class="footer">
    <div>
        <ul class="foote_bottom_ul_amrc">
            <li><a href="/">Noticias</a></li>
            <li><a href="/colaboradores">Colaboradores</a></li>
            <li><a href="/donaciones">Donaciones</a></li>
        </ul>
        <p class="text-center">Copyright @2019  | <a href="https://buscatcu.ucr.ac.cr/buscatcu/boleta/4328?palabra=663&disciplina=93">TCU 663</a></p>
    </div>
</footer>
<?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\layouts\footer.blade.php ENDPATH**/ ?>