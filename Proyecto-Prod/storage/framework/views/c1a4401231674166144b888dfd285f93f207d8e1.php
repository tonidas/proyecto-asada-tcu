<?php $__env->startSection('links'); ?>
<link rel="stylesheet" href="<?php echo e(asset('libs/typeahead/Typeahead.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('libs/cropper/css/cropper.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/noticia/crear_noticia.css')); ?>">

<script type="text/javascript" src="<?php echo e(asset('libs/cropper/js/cropper.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('libs/tinymce/tinymce.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('titulo', 'Crear noticia'); ?>

<?php $__env->startSection('header'); ?>
##parent-placeholder-594fd1615a341c77829e83ed988f137e1ba96231##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('Navbar'); ?>
<div id="header">
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contenido'); ?>
<div id="crearNoticia"></div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Bibliotecas\Escritorio\proyecto-asada-tcu\Proyecto - copia\resources\views\noticia\crear_noticia.blade.php ENDPATH**/ ?>