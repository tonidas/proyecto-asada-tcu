<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{asset('libs/font-awesome/fontawesome-free-5.10.2-web/css/all.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('libs/bootstrap-4.3.1-dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/general.css')}}">

    @section('header_css')
        <link rel="stylesheet" type="text/css" href="{{asset('css/header.css')}}">
    @show
    @section('footer_css')
        <link rel="stylesheet" type="text/css" href="{{asset('css/footer.css')}}">
    @show

    <script type="text/javascript" src="{{asset('libs/jquery/jquery-3.4.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/popper/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('libs/bootstrap-4.3.1-dist/js/bootstrap.min.js')}}"></script>

    @section('header_js')
        <script type="text/javascript" src="{{asset('js/header.js')}}"></script>
    @show
    @section('footer_js')
    @show

    @yield('links')
    <title>@yield('titulo')</title>
  </head>
  <body>
    @section('header')
        @include('layouts.header')
    @show

    @yield('contenido')

    @section('footer')
        @include('layouts.footer')
    @show
  </body>
</html>

@yield('botLinks')
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
