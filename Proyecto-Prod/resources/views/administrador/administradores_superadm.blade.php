@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/administrador/administradores_superadm.css')}}">
@endsection

@section('titulo', 'Administradores')

@section('header')
@parent
@endsection

@section('contenido')
<div id="administradoresSuperAdm"></div>
@endsection

@section('footer')
@endsection

@section('botLinks')
<link rel="stylesheet" type="text/css" href="{{asset('libs/DataTables/datatables.min.css')}} "/>
<script type="text/javascript" src="{{asset('libs/DataTables/datatables.min.js')}}"></script>
@endsection
