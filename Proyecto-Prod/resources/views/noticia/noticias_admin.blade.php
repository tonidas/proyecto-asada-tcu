@extends('layouts.base')

@section('links')
<link rel="stylesheet" href="{{asset('css/noticia/noticias_admin.css')}}">
@endsection

@section('titulo', 'Noticias')

@section('header')
@parent
@endsection

@section('contenido')
<div id="noticiasAdmin"></div>
@endsection

@section('footer')
@endsection

@section('botLinks')
<link rel="stylesheet" type="text/css" href="{{asset('libs/DataTables/datatables.min.css')}} "/>
<script type="text/javascript" src="{{asset('libs/DataTables/datatables.min.js')}}"></script>
@endsection
