@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/colaborador/editar_colaborador.css')}}">
@endsection

@section('titulo', 'Editar colaborador')

@section('header')
@parent
@endsection

@section('contenido')
<script>
const id = "{{$id}}";
</script>
<div id="editarColaborador"></div>
@endsection

@section('footer')
@endsection
