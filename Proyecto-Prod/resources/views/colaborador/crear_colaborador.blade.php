@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/colaborador/crear_colaborador.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('libs/filepond/filepond.css')}}">

<script type="text/javascript" src="{{asset('libs/validator/validator.js')}}"></script>
@endsection

@section('titulo', 'Crear colaborador')

@section('header')
@parent
@endsection

@section('contenido')
<div id="crearColaborador"></div>
@endsection

@section('footer')
@endsection
