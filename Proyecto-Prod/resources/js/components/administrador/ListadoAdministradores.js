import React from 'react';

const ListadoAdministradores = (props) => (
    <div className={props.classContenedor}>
        <div className="spinner-cont">
            <div className="spinner-border text-primary" role="status">
                <span className="sr-only"></span>
            </div>
        </div>
        <div>
            <div className="mb-3 row">
                <div className="col-6">
                    {props.idTabla == 'administradores' ? (
                        <button type="button" data-tooltip="tooltip" data-placement="top" title="Desactivar" className="desactivar cont-icono btn btn-outline-danger" disabled><i className="fas fa-ban"></i></button>
                    )
                    : (
                        <button type="button" data-tooltip="tooltip" data-placement="top" title="Restaurar" className="activar cont-icono btn btn-outline-success" disabled><i className="fas fa-undo"></i></button>
                    )}
                </div>
                {props.idTabla == 'administradores' ? (
                    <div className="col-6">
                        <a id="agregar" className="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Crear administrador" href="administrador_crear"><i className="far fa-plus-square"></i></a>
                        <a id="mostrarInactivos" className="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" data-toggle="modal" data-target="#modalAdminInactivos" title="Mostrar inactivos" href="false"><i className="fas fa-list"></i></a>
                    </div>
                )
                    : (null)
                }
            </div>
            <div className="mb-5">
                <table id={props.idTabla} className="table table-striped table-bordered dt-responsive display">
                    <thead>
                        <tr>
                            {props.idTabla == 'administradores' ? (
                                <th className="multi-check-cont">
                                    <div className="custom-control custom-checkbox">
                                        <input type="checkbox" className="custom-control-input" id="multi-check-adm-activos"/>
                                        <label className="custom-control-label ml-2" htmlFor="multi-checkbox"></label>
                                    </div>
                                </th>
                            ) : (
                                <th className="multi-check-cont">
                                    <div className="custom-control custom-checkbox">
                                        <input type="checkbox" className="custom-control-input" id="multi-check-adm-inactivos"/>
                                        <label className="custom-control-label ml-2" htmlFor="multi-checkbox"></label>
                                    </div>
                                </th>
                            )}
                            <th>Cédula</th>
                            <th>Correo</th>
                            <th>Nombre</th>
                            <th>Teléfono</th>
                            {props.idTabla == 'administradores' ? (<th></th>) : null}
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
)
export default ListadoAdministradores
