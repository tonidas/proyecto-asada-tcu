import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Swal from 'sweetalert2'
import ListadoAdministradores from './ListadoAdministradores';

const axios = require('axios');
const crsf = $('meta[name=csrf-token]').attr("content");
const url = url_g;
const tablaAdmin = "#administradores";
const tablaAdminInactivos = "#administradoresInactivos";

const sleep = (milliseconds) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

export default class AdministradoresSuperAdm extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="container mt-5 mb-5">
                <h3 className="color-1">Administradores</h3>
                <hr></hr>
                <ListadoAdministradores
                    classContenedor="listadoAdministradores"
                    idTabla="administradores"
                />
                <div id="modalAdminInactivos" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-xl full-v">
                        <div className="modal-content">
                            <div className="modal-body container">
                                <h3 className="color-1">Administradores inactivos</h3>
                                <hr></hr>
                                <ListadoAdministradores
                                    classContenedor="listadoAdministradoresInactivos"
                                    idTabla="administradoresInactivos"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const configTablaAdminInactivos = {
    "table-layout": "fixed",
    "language": {
        "url": url + "libs/DataTables/language/Spanish.json"
    },
    "ajax": function(data, callback, settings) {
        var res = axios.get(url + 'obtener_administradores_inactivos')
            .then((res) => {
                //Esperar antes de mostrar la lista
                sleep(500)
                    .then(() => {
                        callback({ 'data': res.data });
                        //Ocultar carga y mostrar listado, si sel modal esta siendo mostrado, para evitar fallos en la animacion
                        if (($("#modalAdminInactivos").data('bs.modal') || {})._isShown) {
                            $(".listadoAdministradoresInactivos .spinner-cont").css("visibility", "hidden");
                            $(".listadoAdministradoresInactivos").css("visibility", "visible");
                        }
                    });
            })
            .catch((e) => {
            })
    },
    "order": [
        [1, 'asc']
    ],
    "responsive": false,
    "scrollY": true,
    "scrolX": true,
    "scrollCollapse": true,
    "paging": true,
    "columns": [
        {
            "data": null
        },
        {
            "data": "cedula"
        },
        {
            "data": "correo"
        },
        {
            "data": "nombre_completo"
        },
        {
            "data": "telefono"
        }],
    "columnDefs": [
        {
            "targets": 0,
            "width": "5%",
            "orderable": false,
            "className": "text-center",
            "render": function(data, type, row) {
                return (
                    '<div class="custom-control custom-checkbox indi-check-cont">' +
                    '<input type="checkbox" class="custom-control-input indi-check" id="indi-check-' + data.id + '"/>' +
                    '<label class="custom-control-label ml-2" for="indi-check-' + data.id + '"></label>' +
                    '</div>'
                );
            },
        }],
    "preDrawCallback": function(settings) {
        //Reiniciar checks y desactivar opciones
        $("#multi-check-adm-inactivos").prop("checked", false);
        $(tablaAdminInactivos).closest(".dataTables_scrollBody").find(".indi-check").prop('checked', false);
        $(".activar").prop("disabled", true);
    },
    "drawCallback": function(settings) {
        //Reinicializar los tooltip
        $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });
    }
}

$(document).ready(function() {

    if (document.getElementById('administradoresSuperAdm')) {
        ReactDOM.render(<AdministradoresSuperAdm />, document.getElementById('administradoresSuperAdm'));
        $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });
        //Manejo de check - global
        $(".multi-check-cont, .multi-check-cont input[type=checkbox]").on("click", function(e) {
            var tableHead = $(this).closest("table")[0];
            var tableBody = $(this).closest(".dataTables_scrollHead").next(".dataTables_scrollBody").find("table")[0];
            var indiChecks = $(tableBody).find(".indi-check");
            var multiCheck = $(tableHead).find(".multi-check-cont input[type=checkbox]")[0];
            //Verificar que exista almenos un elemento en la tabla
            if (indiChecks.length > 0) {
                var change = false;

                if ($(multiCheck).prop('checked')) {
                    $(multiCheck).prop('checked', false);
                    $(indiChecks).prop('checked', false);
                    //Deshabilitar opciones
                    change = true;
                } else {
                    $(multiCheck).prop('checked', true);
                    $(indiChecks).prop('checked', true);
                    //Habilitar opciones
                }
                //Cambiar opciones
                if ("#" + tableBody.id == tablaAdmin) {
                    $(".desactivar").prop("disabled", change);
                }
                if ("#" + tableBody.id == tablaAdminInactivos) {
                    $(".activar").prop("disabled", change);
                }
            }
        });
        //Manejo de check - individual
        $(document).on('click', 'table tbody tr td:first-child', function(e) {
            e.preventDefault();
            var target;

            if (e.target.tagName == "LABEL") {
                target = $(e.target).parent().find(".indi-check")[0];
            } else if (e.target.tagName == "TD" || e.target.tagName == "DIV") {
                target = $(e.target).find(".indi-check")[0];

            }
            singleCheckHandler(target);
        });

        function singleCheckHandler(target) {
            var tableHead = $(target).closest(".dataTables_scrollBody").prev(".dataTables_scrollHead")[0];
            var tableBody = $(target).closest("table")[0];
            var multiCheck = $(tableHead).find(".multi-check-cont input[type=checkbox]")[0];
            //Con check, quitar check individual y global
            if (target.checked) {
                $(target).prop('checked', false);
                $(multiCheck).prop('checked', false);
                //Sin check, agregar check inidivual
            } else {
                $(target).prop('checked', true);
            }
            var totalChecks = $(tableBody).find(".indi-check").length;
            var checked = $(tableBody).find(".indi-check:checked").length;
            //Si todos los checks individuales estan activos, activar el global
            if (checked == totalChecks) {
                $(multiCheck).prop('checked', true);
            }
            //Si hay mas de un check individual, activar opciones
            if ("#" + tableBody.id == tablaAdmin) {

                if (checked > 0) {
                    $(".desactivar").prop("disabled", false);
                } else {
                    $(".desactivar").prop("disabled", true);
                }
            }

            if ("#" + tableBody.id == tablaAdminInactivos) {

                if (checked > 0) {
                    $(".activar").prop("disabled", false);
                } else {
                    $(".activar").prop("disabled", true);
                }
            }
        }

        $("#modalAdminInactivos").on("show.bs.modal", function() {
        });

        $("#modalAdminInactivos").on("shown.bs.modal", function() {
            //Mostrar carga
            $(".listadoAdministradoresInactivos .spinner-cont").css("visibility", "visible");
            //Inicializar tabla de ser necesario
            if (!$.fn.DataTable.isDataTable(tablaAdminInactivos)) {
                $(tablaAdminInactivos).DataTable(configTablaAdminInactivos);
            } else {
                //De lo contrario recargar su contenido
                $(tablaAdminInactivos).DataTable().ajax.reload();
            }
        });

        $("#modalAdminInactivos").on("hide.bs.modal", function() {
            //Ocultar contenedor de listado
            $(".listadoAdministradoresInactivos").css("visibility", "hidden");
        });

        $(tablaAdmin).DataTable({
            "language": {
                "url": url + "libs/DataTables/language/Spanish.json"
            },
            "ajax": function(data, callback, settings) {
                //Mostrar carga si es la primera vez
                if ($(".listadoAdministradores tbody tr td").not("td.dataTables_empty").parent().length == 0) {
                    $(".listadoAdministradores .spinner-cont").css("visibility", "visible");
                }
                var res = axios.get(url + 'obtener_administradores_activos')
                    .then((res) => {
                        //Esperar antes de mostrar la lista
                        sleep(500)
                            .then(() => {
                                callback({ 'data': res.data });
                                //Mostrar header si se trata de la primera carga
                                if ($(".listadoAdministradores .spinner-cont").css("visibility") == "visible") {
                                    //Ocultar carga y mostrar listado
                                    $(".listadoAdministradores .spinner-cont").css("visibility", "hidden");
                                    $(".listadoAdministradores").css("visibility", "visible");
                                }
                            });
                    })
                    .catch((e) => {
                    })
            },
            "order": [
                [1, 'asc']
            ],
            "responsive": false,
            "scrollY": true,
            "scrolX": true,
            "scrollCollapse": true,
            "paging": true,
            "columns": [
                {
                    "data": null
                },
                {
                    "data": "cedula"
                },
                {
                    "data": "correo"
                },
                {
                    "data": "nombre_completo"
                },
                {
                    "data": "telefono"
                },
                {
                    "data": null
                }
            ],
            "columnDefs": [
                {
                    "targets": 1,
                    "render": function(data, type, row) {
                        data = data.slice(0, 1) + "-" + data.slice(1);
                        data = data.slice(0, 6) + "-" + data.slice(6);
                        return data;
                    },
                },
                {
                    "targets": 4,
                    "render": function(data, type, row) {
                        return data.slice(0, 4) + "-" + data.slice(4);
                    },
                },
                {
                    "targets": 0,
                    "width": "5%",
                    "orderable": false,
                    "className": "text-center",
                    "render": function(data, type, row) {

                        if (!data.isSuperUsuario) {
                            return (
                                '<div class="custom-control custom-checkbox indi-check-cont">' +
                                '<input type="checkbox" class="custom-control-input indi-check" id="indi-check-' + data.id + '"/>' +
                                '<label class="custom-control-label ml-2" for="indi-check-' + data.id + '"></label>' +
                                '</div>'
                            );
                        }
                        return ("");
                    },
                },
                {
                    "targets": 5,
                    "orderable": false,
                    "className": "text-center bg-white",
                    "render": function(data, type, row) {
                        return '<a data-tooltip="tooltip" data-placement="top" title="Editar" class="cont-icono editar btn btn-outline-info" href="administrador_editar/' + data.id + '"><i class="far fa-edit"></i></a>';
                    },
                }],
            "preDrawCallback": function(settings) {
                //Reiniciar checks y desactivar opciones
                $("#multi-check-adm-activos").prop("checked", false);
                $(tablaAdmin).closest(".dataTables_scrollBody").find(".indi-check").prop('checked', false);
                $(".desactivar").prop("disabled", true);
            },
            "drawCallback": function(settings) {
                //Reinicializar los tooltip
                $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });
            }
        });

        $(document).on("click", '.desactivar', function(e) {
            var fila = $(this).parent().parent();

            Swal.fire({
                title: 'Confirmación',
                text: "¿Estas seguro de desactivar el administrador seleccionado?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: (respuesta) => {

                    if (respuesta) {
                        var ids = [];
                        var data = $(tablaAdmin).DataTable().rows(function(idx, data, node) {
                            var condicion = $(node).find(".indi-check")[0];
                            condicion = typeof condicion == "undefined" ? false : condicion.checked;
                            return condicion == true ? true : false;
                        }).data();

                        for (var i = 0; i < data.length; i++) {
                            ids.push(data[i].id);
                        }
                        return axios.post(url + 'administradores_desactivar',
                            {
                                "ids": ids,
                            })
                            .then((res) => {
                                return res.data;
                            })
                            .catch((e) => {
                                return "Error";
                            });
                    }
                }
            })
                .then((data) => {

                    if (data.value == "Error") {
                        Toast.fire({
                            type: 'error',
                            title: 'Ha ocurrido un error, intente de nuevo en un momento',
                            timer: 6000
                        });
                    } else if (data.value == null) {
                    } else {
                        $(tablaAdmin).DataTable().rows(function(idx, data, node) {
                            var condicion = $(node).find(".indi-check")[0];
                            condicion = typeof condicion == "undefined" ? false : condicion.checked;
                            return condicion == true ? true : false;
                        }).remove().draw(false);
                        $(".desactivar").prop("disabled", true);
                        Toast.fire({
                            type: 'success',
                            title: 'Desactivado correctamente',
                        });
                    }
                })
                .catch((e) => {
                    Toast.fire({
                        type: 'error',
                        title: 'Ha ocurrido un error, intente de nuevo en un momento',
                        timer: 6000
                    });
                });
        });

        $(".activar").on("click", function(e) {
            Swal.fire({
                title: 'Confirmación',
                text: "¿Estas seguro de restaurar los administradores seleccionados?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: (respuesta) => {

                    if (respuesta) {
                        var ids = [];
                        var data = $(tablaAdminInactivos).DataTable().rows(function(idx, data, node) {
                            return $(node).find(".indi-check")[0].checked == true ? true : false;
                        }).data();

                        for (var i = 0; i < data.length; i++) {
                            ids.push(data[i].id);
                        }
                        return axios.post(url + 'administradores_activar',
                            {
                                "ids": ids,
                            })
                            .then((res) => {
                                return res.data;
                            })
                            .catch((e) => {
                                return "Error";
                            });
                    }
                }
            })
                .then((data) => {

                    if (data.value == "Error") {
                        Toast.fire({
                            type: 'error',
                            title: 'Ha ocurrido un error, intente de nuevo en un momento',
                            timer: 6000
                        });
                    } else if (data.value == null) {
                    } else {
                        //Remover items procesados
                        $(tablaAdminInactivos).DataTable().rows(function(idx, data, node) {
                            return $(node).find(".indi-check")[0].checked == true ? true : false;
                        }).remove().draw(false);
                        //Recargar adminiistradores activos
                        $(tablaAdmin).DataTable().ajax.reload();
                        //Desactivar opciones de ambas tablas
                        $(".activar").prop("disabled", true);
                        $(".desactivar").prop("disabled", true);
                        Toast.fire({
                            type: 'success',
                            title: 'Restaurado correctamente',
                        });
                    }
                })
                .catch((e) => {
                    Toast.fire({
                        type: 'error',
                        title: 'Ha ocurrido un error, intente de nuevo en un momento',
                        timer: 6000
                    });
                });
        });
    }
});
