import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Pagination from "react-js-pagination";

const axios = require('axios');
const url = url_g;

export default class DonacionesUsuario extends Component {

    constructor(props) {
        super(props)

        this.state = {
            donaciones: [],
            activePage: 1,
            itemsCountPerPage: 1,
            totalItemsCount: 1,
            pageRangeDisplayed: 3
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    async componentDidMount() {

        try {
            var res = await axios.get(url + 'obtener_donacion_usuario');

            if (res.data.data.length == 0) {
                $("footer").animate({ opacity: 1 });
            }
            this.setState({
                donaciones: res.data.data,
                itemsCountPerPage: res.data.per_page,
                totalItemsCount: res.data.total,
                activePage: res.data.current_page
            }, () => {

                $(".container").css({ "opacity": "0", "display": "block" }).animate({ opacity: 1 }, () => {

                    if (this.state.totalItemsCount > this.state.itemsCountPerPage) {
                        $(".paginacion").css({ "opacity": "0", "display": "flex" }).animate({ opacity: 1 });
                    }
                    $("footer").animate({ opacity: 1 });
                });
            });
        } catch (e) {

        }
    }

    async componentDidUpdate(prevProps, prevState) {

    }

    async handlePageChange(pageNumber) {

        var res = await axios.get(url + 'obtener_donacion_usuario?page=' + pageNumber);
        this.setState({
            donaciones: res.data.data,
            itemsCountPerPage: res.data.per_page,
            totalItemsCount: res.data.total,
            activePage: res.data.current_page
        });
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div className="container mb-5 mt-5 display-none opacity-0">
                <div className="mb-5">
                    <h2 className="encabezado display-4 color-2 text-center">Donaciones</h2>
                </div>
                <div className="donaciones mb-5 row" >
                    {this.state.donaciones.map((donacion, i) =>
                        <div className="donacion col-sm-12 col-md-6 col-lg-6 col-xl-6" key={donacion.numero_cuenta}>
                            <div className="card">
                                <div className="card-body">
                                    <p className="card-text"><strong>Banco:</strong> {donacion.nombre_banco}</p>
                                    <p className="card-text"><strong>Número de cuenta:</strong> {donacion.numero_cuenta.slice(0, 3) + "-" + donacion.numero_cuenta.slice(3)}</p>
                                    <p className="card-text"><strong>Propietario:</strong> {donacion.nombre_propietario}</p>
                                    <p className="card-text"><strong>Moneda:</strong> {donacion.moneda}</p>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                <div className="paginacion justify-content-center display-none">
                    <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemsCountPerPage}
                        totalItemsCount={this.state.totalItemsCount}
                        pageRangeDisplayed={this.state.pageRangeDisplayed}
                        onChange={this.handlePageChange}
                        itemClass='page-item'
                        linkClass='page-link'
                    />
                </div>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('donacionesUsuario')) {
        ReactDOM.render(<DonacionesUsuario />, document.getElementById('donacionesUsuario'));
        //Observer
        const observer = new MutationObserver((mutationList) => {
            mutationList.forEach((mutation) => {

                if (mutation.addedNodes.length) {
                  $(mutation.addedNodes[0]).find(".card").animate({ "opacity": "1" }, 500);
                }
            })
        });
        const donaciones = document.querySelector(".donaciones") // elemento que queremos observar
        // Opcions para el observer
        const observerOptions = {
            attributes: false,
            childList: true,
            subtree: false,
            characterData: false,
            attributeOldValue: false,
            characterDataOldValue: false
        };
        observer.observe(donaciones, observerOptions);
    }
});
