import React from 'react';

const ListadoDonaciones = (props) => (
    <div id="listadoDonaciones">
        <div className="spinner-cont">
            <div className="spinner-border text-primary" role="status">
                <span className="sr-only"></span>
            </div>
        </div>
        <div className="mb-3">
            <button id="eliminar" type="button" data-tooltip="tooltip" data-placement="top" title="Eliminar selección" className="cont-icono btn btn-outline-danger" disabled><i className="far fa-trash-alt"></i></button>
            <a id="agregar" className="cont-icono btn btn-outline-primary float-right" data-tooltip="tooltip" data-placement="top" title="Crear donación" href="donaciones_crear"><i className="far fa-plus-square"></i></a>
        </div>
        <div className="mb-5">
            <table id="donaciones" className="table table-striped table-bordered dt-responsive display">
                <thead>
                    <tr>
                        <th className="multi-check-cont">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="multi-check" />
                                <label className="custom-control-label ml-2" htmlFor="multi-checkbox"></label>
                            </div>
                        </th>
                        <th>Nombre propietario</th>
                        <th>Nombre de banco</th>
                        <th>Numero cuenta</th>
                        <th>Moneda</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
)
export default ListadoDonaciones
