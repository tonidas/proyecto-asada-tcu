import React from 'react';
import InputMask from 'react-input-mask';

const FormDonacion = (props) => (
    <form>
        <div className="form-group">
            <label htmlFor="nombre_propietario" className="font-weight-bold">Nombre del propietario</label>
            <input type="text" name="nombre_propietario" className="form-control" maxLength="50" value={props.form.nombre_propietario} onChange={props.onChange} />
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group">
            <label htmlFor="numero_cuenta" className="font-weight-bold">Número de cuenta</label>
            {props.edicion ?
                  <input type="text" value={props.numero_cuenta} className="form-control-plaintext" readOnly/>
              :
              <InputMask
                  className="form-control"
                  name="numero_cuenta"
                  mask="999-99999999999999999"
                  value={props.form.numero_cuenta}
                  onChange={props.onChange}
                  minLength="21"
                  maskChar=""
                  alwaysShowMask={false}
              />
            }
            <div className="invalid-feedback"></div>
            <small id="ayuda-numero_cuenta" className="form-text text-muted">Veinte dígitos, sin cero al inicio ni guiones</small>
        </div>
        <div className="form-group">
            <label htmlFor="nombre_banco" className="font-weight-bold">Nombre del banco</label>
            <input type="text" name="nombre_banco" className="form-control" maxLength="50" value={props.form.nombre_banco} onChange={props.onChange} />
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group">
            <label htmlFor="moneda" className="font-weight-bold">Tipo de cuenta (moneda)</label>
            <input type="text" name="moneda" className="form-control" maxLength="25" value={props.form.moneda} onChange={props.onChange} />
            <div className="invalid-feedback"></div>
            <small id="ayuda-moneda" className="form-text text-muted">Colones, Dolares, Euros...</small>
        </div>
        {props.edicion ?
          <button id="actualizar" type="submit" className="btn btn-success" onClick={props.onSubmit}>Actualizar</button>
        :
          <button id="guardar" type="submit" className="btn btn-success" onClick={props.onSubmit}>Guardar</button>
        }
    </form>
)
export default FormDonacion
