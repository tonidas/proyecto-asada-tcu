import React, {
    Component
} from 'react';
import ReactDOM from 'react-dom';
import Swal from 'sweetalert2'
import validator from 'validator';

const axios = require('axios');
const crsf = $('meta[name=csrf-token]').attr("content");
const url = url_g;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

export default class InicioSesion extends Component {

    constructor(props) {
        super(props)
        this.imagesDragDropPreview = React.createRef();

        this.state = {
            form: {
                correo: "",
                password: ""
            },
            formErrors: {
                correo: "",
                password: ""
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const elemento = e.target;
        const nombre = e.target.name;
        const valor = e.target.value;
        let formErrors = this.state.formErrors;

        switch (nombre) {
            case 'correo':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.correo = "Este espacio es requerido";

                } else if (!validator.isEmail(valor.trim())) {
                    formErrors.correo = "No se ha ingresado un correo electrónico valido";

                } else {
                    formErrors.correo = "";
                }
                break;
            case 'password':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.password = "Este espacio es requerido";

                } else if (valor.length < $(elemento).attr("minLength")) {
                    formErrors.password = "El tamaño mínimo es " + $(elemento).attr("minLength");

                } else {
                    formErrors.password = "";
                }
                break;
        }

        if (formErrors[nombre] != "") {
            $(elemento).removeClass("is-valid");
            $(elemento).addClass("is-invalid");
            $(elemento).parent().find(".invalid-feedback").html(formErrors[nombre]).show();
        } else {
            $(elemento).removeClass("is-invalid");
            $(elemento).addClass("is-valid");
            $(elemento).parent().find(".invalid-feedback").html("").hide();
        }
        this.setState({
            form: {
                ...this.state.form,
                [nombre]: valor
            },
            formErrors
        })
    }

    async handleSubmit(e) {
        e.preventDefault();
        const ref = this;
        //Verificar formulario
        Object.keys(this.state.form).forEach((key) => {
            let elemento = $("[name=" + key + "]")[0];
            this.handleChange({
                target: elemento
            });
        });

        if ($("form .is-valid").length == Object.values(this.state.form).length) {
            //Desactivar formulario y botones de agregar
            $(".container .form-control, .btn").prop("disabled", true);
            //Enviar contenido al servidor
            axios.post(url + 'autentificar', {
                "correo": this.state.form.correo,
                'password': this.state.form.password
            })
                .then(function(response) {

                    if (response.status == 200) {
                        window.location.href = url + "noticia";
                    }
                })
                .catch(function(e) {

                    if (e.response.data.erroresForm != null) {
                        const erroresForm = e.response.data.erroresForm;
                        let formErrors = ref.state.formErrors;

                        Object.keys(erroresForm).forEach((nombreInput) => {
                            //Asignar el primer error de cada input
                            formErrors[nombreInput] = erroresForm[nombreInput][0];
                            const elemento = $("[name=" + nombreInput + "]");

                            if (formErrors[nombreInput] != "") {
                                elemento.removeClass("is-valid");
                                elemento.addClass("is-invalid");
                                elemento.parent().find(".invalid-feedback").html(formErrors[nombreInput]).show();
                            } else {
                                elemento.removeClass("is-invalid");
                                elemento.addClass("is-valid");
                                elemento.parent().find(".invalid-feedback").html("").hide();
                            }
                        });

                        ref.setState({
                            formErrors
                        }, () => {
                            $(".is-invalid").parent()[0].scrollIntoView();
                        })
                    } else {
                        Toast.fire({
                            type: 'error',
                            title: 'Error al iniciar sesión, intente de nuevo en un momento',
                            timer: 6000
                        });
                    }
                    //Activar formulario y botones de agregar
                    $(".container .form-control, .btn").prop("disabled", false);
                });
        } else {
            $(".is-invalid").parent()[0].scrollIntoView();
        }
    }

    render() {
        return (
            <div className="container mt-5 mb-5">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Acceder</div>

                            <div className="card-body">
                                <form method="POST">

                                    <div className="form-group row">
                                        <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Correo electrónico</label>
                                        <div className="col-md-6">
                                            <input id="correo" type="email" className="form-control" name="correo" maxLength="100" onChange={this.handleChange} value={this.state.form.correo} autoComplete="email" autoFocus />
                                            <div className="invalid-feedback" role="alert"></div>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="password" className="col-md-4 col-form-label text-md-right">Contraseña</label>
                                        <div className="col-md-6">
                                            <input id="password" type="password" className="form-control" name="password" minLength="10" maxLength="20" onChange={this.handleChange} value={this.state.form.password} autoComplete="current-password" />
                                            <div className="invalid-feedback" role="alert"></div>
                                        </div>
                                    </div>

                                    <div className="form-group row mb-0">
                                        <div className="col-md-8 offset-md-4">
                                            <button type="button" onClick={this.handleSubmit} className="btn btn-primary">Iniciar sesión</button>
                                            <a className="btn btn-link" href="password/reset">¿Olvidaste tu contraseña?</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('inicioSesion')) {
        ReactDOM.render(< InicioSesion />, document.getElementById('inicioSesion'));
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
        });
        $("input, textarea").on("drop", function() {
            return false
        });
    }
});
