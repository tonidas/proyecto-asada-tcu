import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Pagination from "react-js-pagination";

const axios = require('axios');
const url = url_g;

const sleep = (milliseconds) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
const scrollToElement = (element) => {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(element).offset().top - $("nav").height()
    }, 0);
};

export default class NoticiasUsuario extends Component {

    constructor(props) {
        super(props)

        this.state = {
            noticias: [],
            noticiasRecientes: [],
            activePage: 1,
            itemsCountPerPage: 6,
            totalItemsCount: 1,
            pageRangeDisplayed: 3,
            renderOrd: {
                noticiasRecientes: false,
                noticias: false
            }
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    async componentDidMount() {

        try {
            let res = await axios.get(url + 'obtener_noticias_recientes/' + 3);
            this.setState({
                noticiasRecientes: res.data
            });

            if (res.data.length == 0) {
                $("footer").animate({ opacity: 1 });
            }

            res = await axios.get(url + 'obtener_noticias_usuario?paginaActual=' + this.state.activePage + '&totalPagina=' + this.state.itemsCountPerPage);

            if (res.data.noticias.length == 0) {
                $(".spinner-page-cont").css("visibility", "hidden");
            }

            this.setState({
                ...this.state,
                noticias: res.data.noticias,
                itemsCountPerPage: res.data.totalPagina,
                totalItemsCount: res.data.total,
                activePage: this.state.activePage
            });
        } catch (e) {
        }
    }

    async componentDidUpdate(prevProps, prevState) {

        if (this.state.noticiasRecientes.length > 0 && prevState.noticiasRecientes != this.state.noticiasRecientes) {

            if ($(".noticia-principal, .noticia-secundaria").length == this.state.noticiasRecientes.length) {
                $(".noticias-recientes").css({ "opacity": "0", "display": "flex" }).animate({ opacity: 1 }, () => {

                    if (this.state.noticias.length > 0) {

                        if ($(".noticia-pagina").length == this.state.noticias.length) {
                            $(".m-noticias").css({ "opacity": "0", "display": "block" }).animate({ opacity: 1 });
                            $(".noticias").css({ "opacity": "1", "display": "flex" });

                            if (this.state.totalItemsCount > this.state.itemsCountPerPage) {
                                $(".paginacion").css({ "opacity": "0", "display": "flex" }).animate({ opacity: 1 });
                            }
                        }
                    }
                    $("footer").animate({ opacity: 1 });
                });
            }
        }
    }

    async handlePageChange(paginaActual) {

        try {
            //Obtener noticias de la siguiente pagina
            const res = await axios.get(url + 'obtener_noticias_usuario?paginaActual=' + paginaActual + '&totalPagina=' + this.state.itemsCountPerPage);
            scrollToElement(".m-noticias");
            this.setState({
                ...this.state,
                noticias: res.data.noticias,
                itemsCountPerPage: res.data.totalPagina,
                totalItemsCount: res.data.total,
                activePage: paginaActual
            });

        } catch (e) {
        }
    }

    plantillaNoticia(cols, noticia, clases, i) {
        let $img = noticia.img_principal;

        if (clases == "noticia-principal") {
            $img = noticia.img_principal.replace("/img_o/", "/img_md/");
        } else {
            $img = noticia.img_principal.replace("/img_o/", "/img_sm/");
        }
        return (
            <div className={"col-" + cols[0] + " col-sm-" + cols[1] + " col-md-" + cols[2] + " col-lg-" + cols[3] + " col-xl-" + cols[4] + " noticia"} key={noticia.id}>
                <a href={"noticia_u/" + noticia.id}>
                    <div className={"card " + clases + (clases == "noticia-pagina" ? " display-none" : "")}>
                        <div className="cont-img">
                            <img src={$img} className="card-img-top imagen" alt="..." />
                        </div>
                        <div className="card-body">
                            <div>
                                <p className="post-title titulo">{noticia.titulo}</p>
                                {clases != "noticia-pagina" ? <p className="card-text descripcion">{noticia.descripcion}</p> : (null)}
                            </div>
                            <div>
                                <hr className="hr-noticia" />
                                <p className="text-muted fecha">Actualizada {noticia.fecha_actualizacion_dif}</p>
                            </div>
                        </div>
                    </div >
                </a>
            </div >
        )
    }

    render() {
        return (
            <div className="container mb-5 mt-5">
                <div className="spinner-page-cont">
                    <div className="spinner-border text-primary" role="status">
                        <span className="sr-only"></span>
                    </div>
                </div>
                <div className="row noticias-recientes display-none opacity-0">
                    <div className="col-sm-12 col-md-8  recientes-izq">
                        <div className="row">
                            {this.state.noticiasRecientes.length > 0 ? this.plantillaNoticia([12, 12, 12, 12, 12], this.state.noticiasRecientes[0], "noticia-principal", 0) : (null)}
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-4  recientes-der">
                        <div className="row">
                            {this.state.noticiasRecientes.length > 1 ? this.plantillaNoticia([12, 12, 12, 12, 12], this.state.noticiasRecientes[1], "noticia-secundaria", 1) : (null)}
                        </div>
                        <div className="row">
                            {this.state.noticiasRecientes.length > 2 ? this.plantillaNoticia([12, 12, 12, 12, 12], this.state.noticiasRecientes[2], "noticia-secundaria", 2) : (null)}
                        </div>
                    </div>
                </div>

                <div className="m-noticias display-none opacity-0">
                    <hr className="hr-m-noticia" />
                    <h5>Más noticias</h5>
                    <hr className="hr-m-noticia" />
                </div>

                <div className="row noticias display-none opacity-0 mb-5">
                    {this.state.noticias.map((noticia, i) => { return this.plantillaNoticia([12, 12, 6, 4, 4], noticia, "noticia-pagina", i) })}
                </div>

                <div className="paginacion justify-content-center display-none">
                    <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemsCountPerPage}
                        totalItemsCount={this.state.totalItemsCount}
                        pageRangeDisplayed={this.state.pageRangeDisplayed}
                        onChange={this.handlePageChange}
                        itemClass='page-item'
                        linkClass='page-link'
                    />
                </div>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('noticiasUsuario')) {
        ReactDOM.render(<NoticiasUsuario />, document.getElementById('noticiasUsuario'));
        //Observer
        const observer = new MutationObserver((mutationList) => {
            mutationList.forEach((mutation) => {

                if (mutation.addedNodes.length) {
                    $(mutation.addedNodes[0]).find(".card").css({ "display": "block", "opacity": "0" }).animate({ opacity: 1 }, 500);
                    $(".spinner-page-cont").css("visibility", "hidden");
                }
            })
        });
        const noticias = document.querySelector(".noticias") // elemento que queremos observar
        // Opcions para el observer
        const observerOptions = {
            attributes: false,
            childList: true,
            subtree: false,
            characterData: false,
            attributeOldValue: false,
            characterDataOldValue: false
        };
        observer.observe(noticias, observerOptions);
    }
});
