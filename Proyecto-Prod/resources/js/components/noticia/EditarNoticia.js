import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import CargadorImagen from './CargadorImagen';
import VizualizadorCropper from './VizualizadorCropper';
import ContenidoNoticia from './ContenidoNoticia';
import 'react-image-crop/dist/ReactCrop.css';
import ReactCrop from 'react-image-crop';
import Swal from 'sweetalert2'
import { Typeahead } from 'react-bootstrap-typeahead';
import validator from 'validator';

const axios = require('axios');
const imageType = require('image-type');

const expresionImg = "image\/(jpeg|png)$";
const crsf = $('meta[name=csrf-token]').attr("content");
const maximo = 5e+6;
const url = url_g;
const urlImgService = urlImgService_g;
const sleep = (milliseconds) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
const scrollToElement = (element) => {
    $([document.documentElement, document.body]).animate({
            scrollTop: $(element).offset().top - $("nav").height()
    }, 0);
};

let cropper;
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

export default class EditarNoticia extends Component {

    constructor(props) {
        super(props)

        this.state = {
            colaboradores: [],
            colaboradoresEstado: false,
            colaboradoresSel: [],
            imagen: "",
            form: {
                titulo: "",
                descripcion: ""
            },
            formErrors: {
                titulo: "",
                descripcion: ""
            },
            //Img
            img: null,
            imgBlob: null,
            ext: "",
            cargado: true,
            primeraCarga: 0,
            //Cropper
            crop: {
                aspect: 16 / 9,
            },
            src: "",
            cropEstado: false,
        }
        this.onImageLoaded = this.onImageLoaded.bind(this);
        this.onCropChange = this.onCropChange.bind(this);
        this.setEstadoCargas = this.setEstadoCargas.bind(this);
        this.onChangeCargaImagenLocal = this.onChangeCargaImagenLocal.bind(this);
        this.onClickCargarImagenOnline = this.onClickCargarImagenOnline.bind(this);
        this.verificador = this.verificador.bind(this);
        this.procesarImagen = this.procesarImagen.bind(this);
        this.activarAcciones = this.activarAcciones.bind(this);
        this.desactivarAcciones = this.desactivarAcciones.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const elemento = e.target;
        const nombre = e.target.name;
        const valor = e.target.value;
        let formErrors = this.state.formErrors;

        switch (nombre) {
            case 'titulo':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.titulo = "Este espacio es requerido";

                } else {
                    formErrors.titulo = "";
                }
                break;
            case 'descripcion':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.descripcion = "Este espacio es requerido";
                } else {
                    formErrors.descripcion = "";
                }
                break;
        }
        if (formErrors[nombre] != "") {
            $(elemento).removeClass("is-valid");
            $(elemento).addClass("is-invalid");
            $(elemento).parent().find(".invalid-feedback").html(formErrors[nombre]).show();
        } else {
            $(elemento).removeClass("is-invalid");
            $(elemento).addClass("is-valid");
            $(elemento).parent().find(".invalid-feedback").html("").hide();
        }
        this.setState({
            form: {
                ...this.state.form,
                [nombre]: valor
            },
            formErrors
        })
    }

    async componentDidMount() {

        try {
            let res = await axios.get(url + 'obtener_noticia_adm?id=' + id);
            this.onClickCargarImagenOnline(null, res.data.img_principal);
            this.setState({
                noticiaActual: res.data,
                colaboradoresSel: res.data.colaboradores,
                form: {
                    titulo: res.data.titulo,
                    descripcion: res.data.descripcion
                },
                imagen: res.data.img_principal
            });
            $("#cuerpo").val(res.data.cuerpo);
            this.inicializarTinyMCE();

            res = await axios.get(url + 'obtener_colaboradores_adm');
            this.setState({
                colaboradores: res.data
            });
        } catch (e) {
        }
    }

    inicializarTinyMCE() {
        tinymce.init({
            selector: '#cuerpo',
            width: "100%",
            height: "600px",
            language: 'es',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media link anchor codesample | ltr rtl',
            menubar: 'file edit view insert format tools table help',
            toolbar_sticky: true,
            toolbar_drawer: 'sliding',
            autoresize_bottom_margin: 50,
            min_height: 700,
            autosave_interval: "20s",
            paste_data_images: true,
            convert_urls: true,
            images_upload_handler: function(blobInfo, success, failure) {
                var xhr, formData;
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', urlImgService + 'procesar.php');

                xhr.onload = function() {
                    var json;

                    if (xhr.status != 200) {
                        //Error HTTP
                        failure('Error del servidor: ' + xhr.status);
                        Toast.fire({
                            type: 'error',
                            title: 'Error al subir una imagen, intente de nuevo en un momento',
                            timer: 6000,
                        });
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location != 'string') {
                        //Error en el formato del JSON
                        failure('Formato JSON invalido: ' + xhr.responseText);
                        Toast.fire({
                            type: 'error',
                            title: 'Error al subir una imagen, intente de nuevo en un momento',
                            timer: 6000,
                        });
                        return;
                    }
                    success(json.location);
                };

                xhr.onerror = function(e) {
                    Toast.fire({
                        type: 'error',
                        title: 'Error al subir una imagen, intente de nuevo en un momento',
                        timer: 6000,
                    });
                }
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                xhr.send(formData);
            },
            init_instance_callback: function() {
                $(".tiny-container").animate({ opacity: 1 }, 300);
            }
        });
    }

    setEstadoCargas(estado) {
        $("#buscarLocal").attr("disabled", estado);
        $("#cargarEnlace").attr("disabled", estado);
        $("#actualizar").attr("disabled", estado);
    }

    onClickCargaImagen(e) {
        $("#cargadorImagen").find("input[type=file]").trigger("click");
    }

    onChangeCargaImagenLocal(e) {
        const ref = this;
        const imagen = $("#cargadorImagen").find("input[type=file]")[0].files[0];
        const verificarExtension = imagen.type.match(expresionImg);
        const fr = new FileReader();
        const boton = $("#buscarLocal");
        this.setEstadoCargas(true);
        boton.html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Cargando');

        if (imagen.size >= maximo) {
            Toast.fire({
                type: 'error',
                title: 'La imagen supera el tamaño maximo permitido' + maximo / 1e+6 + 'MB',
                timer: 6000
            });
            boton.html("Buscar");
            this.setEstadoCargas(false);
            return;
        }

        fr.onload = function() {
            URL.revokeObjectURL(ref.state.src);
            const tipo = imageType(fr.result);
            const blob = new Blob([fr.result], { type: tipo.mime });
            $("#contenedorImg").animate({ opacity: 0 }, 500, function() {
                ref.setState({
                    src: URL.createObjectURL(blob),
                    ext: tipo.ext,
                    imgBlob: blob
                });
                Toast.fire({
                    type: 'success',
                    title: 'Cargado correctamente'
                });
                boton.html("Buscar");
                ref.setEstadoCargas(false);
            });
        }

        fr.onerror = function() {
            Toast.fire({
                type: 'error',
                title: 'Error al cargar ',
                timer: 6000
            });
            boton.html("Buscar");
            ref.setEstadoCargas(false);
        }

        if (verificarExtension == null) {
            Toast.fire({
                type: 'error',
                title: 'Extensión invalida',
                timer: 6000
            });
            boton.html("Buscar");
            this.setEstadoCargas(false);
            return;
        }
        fr.readAsArrayBuffer(imagen);
    }

    onClickCargarImagenOnline(e, enlaceI) {
        const ref = this;
        const enlace = enlaceI != null ? enlaceI : $("#enlace").val();
        const boton = $("#cargarEnlace");

        if (enlace == "") {
            Toast.fire({
                type: 'warning',
                title: 'No se ha ingresado ningún enlace',
                timer: 6000,
            });
            $("#enlace").focus();
            return;
        }
        this.setEstadoCargas(true);
        boton.html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Cargando');

        sleep(1000).then(() => {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', enlace);
            xhr.responseType = 'arraybuffer';
            xhr.onload = (e) => {

                try {
                    const array = new Uint8Array(e.target.response);
                    const tipo = imageType(array);
                    const verificarExtension = tipo.mime.match(expresionImg);

                    if (verificarExtension == null) {
                        Toast.fire({
                            type: 'error',
                            title: 'Extensión invalida',
                            timer: 6000
                        });
                        boton.html("Cargar");
                        ref.setEstadoCargas(false);
                        return;
                    }
                    const blob = new Blob([array], { type: tipo.mime });

                    if (blob.size >= maximo) {
                        Toast.fire({
                            type: 'error',
                            title: 'La imagen supera el tamaño maximo permitido' + maximo / 1e+6 + 'MB',
                            timer: 6000
                        });
                        boton.html("Cargar");
                        ref.setEstadoCargas(false);
                        return;
                    }

                    $("#contenedorImg").animate({ opacity: 0 }, 500, function() {
                        URL.revokeObjectURL(ref.state.src);
                        ref.setState({
                            src: URL.createObjectURL(blob),
                            ext: tipo.ext,
                            imgBlob: blob
                        });
                        Toast.fire({
                            type: 'success',
                            title: 'Cargado correctamente'
                        });
                        boton.html("Cargar");
                        ref.setEstadoCargas(false);
                    });
                } catch (e) {
                    Toast.fire({
                        type: 'error',
                        title: 'Error al cargar',
                        timer: 6000
                    });
                    boton.html("Cargar");
                    ref.setEstadoCargas(false);
                }
            };

            xhr.onerror = function(e) {
                Toast.fire({
                    type: 'error',
                    title: 'Error al solicitar la imagen',
                    timer: 6000
                });
                boton.html("Cargar");
                ref.setEstadoCargas(false);
            };
            xhr.send();
        });
    }

    activarAcciones() {
        //Desactivar formulario y botones de agregar
        $(".form-control, .btn").prop("disabled", false);
        //Desactivar tinymce
        tinyMCE.activeEditor.setMode('design');
        this.setState({
            //Desactivar colaboradores (typeahead)
            colaboradoresEstado: false,
            //Desactivar crop
            cropEstado: false
        });
    }

    desactivarAcciones() {
        //Desactivar formulario y botones de agregar
        $(".form-control, .btn").prop("disabled", true);
        //Desactivar tinymce
        tinyMCE.activeEditor.setMode('readonly');
        this.setState({
            //Desactivar colaboradores (typeahead)
            colaboradoresEstado: true,
            //Desactivar crop
            cropEstado: true
        });
    }

    async handleSubmit(e) {
        e.preventDefault();
        const ref = this;
        this.desactivarAcciones();
        //Establer carga
        $("#actualizar").html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Actualizando');
        //Verificar formulario
        Object.keys(this.state.form).forEach((key) => {
            let elemento = $("[name=" + key + "]")[0];
            this.handleChange({ target: elemento });
        });

        if ($("form .is-valid").length == Object.values(this.state.form).length) {

            if (!this.verificador()) {
                new Promise((resolve, reject) => {

                    try {
                        //Verificar si la imagen ya fue cargada o es la inicial, de no serlo se inicia la carga
                        if (this.state.cargado == false) {
                            //Obtener la imagen (elemento html)
                            const img = this.state.img;
                            this.setState({
                                crop: {
                                    ...this.state.crop,
                                    widthC: img.width,
                                    heightC: img.height
                                }
                            }, () => {
                                //Iniciar proceso de subida de imagen al servidor (Blob)
                                this.procesarImagen(this.state.imgBlob, resolve, reject);
                            });
                        } else {
                            resolve(this.state.imagen);
                        }
                    } catch (e) {
                        reject(e);
                    }
                })
                    .then((imagen) => {

                        this.setState({
                            imagen: imagen,
                            cargado: true
                        }, () => {
                            //Cargar imagenes que aun se encuentren pendientes
                            tinymce.activeEditor.uploadImages();
                            //Enviar contenido al servidor
                            axios.post(url + 'noticia_actualizar',
                                {
                                    "id": id,
                                    "imagen": this.state.imagen,
                                    "form": this.state.form,
                                    "cuerpo": tinymce.activeEditor.getContent(),
                                    "colaboradores": this.state.colaboradoresSel
                                })
                                .then(function(response) {

                                    if (response.status == 200) {
                                        tinymce.activeEditor.settings.autosave_ask_before_unload = false;
                                        window.location.href = url + "noticia";
                                    }
                                })
                                .catch(function(e) {

                                    if (e.response.data.erroresForm != null) {
                                        const erroresForm = e.response.data.erroresForm;
                                        let formErrors = ref.state.formErrors;

                                        Object.keys(erroresForm).forEach((nombreInput) => {
                                            //Asignar el primer error de cada input
                                            formErrors[nombreInput] = erroresForm[nombreInput][0];
                                            const elemento = $("[name=" + nombreInput + "]");

                                            if (formErrors[nombreInput] != "") {
                                                elemento.removeClass("is-valid");
                                                elemento.addClass("is-invalid");
                                                elemento.parent().find(".invalid-feedback").html(formErrors[nombreInput]).show();
                                            } else {
                                                elemento.removeClass("is-invalid");
                                                elemento.addClass("is-valid");
                                                elemento.parent().find(".invalid-feedback").html("").hide();
                                            }
                                        });

                                        ref.setState({
                                            formErrors
                                        }, () => {
                                              scrollToElement($(".is-invalid").parent());
                                        })
                                    } else {
                                        Toast.fire({
                                            type: 'error',
                                            title: 'Error al guardar la noticia, intente de nuevo en un momento',
                                            timer: 6000
                                        });
                                    }
                                    ref.activarAcciones();
                                    $("#actualizar").html("Actualizar");
                                });
                        });
                    })
                    .catch((e) => {
                        Toast.fire({
                            type: 'error',
                            title: 'Error al guardar la noticia, intente de nuevo en un momento',
                            timer: 6000
                        });
                        this.activarAcciones();
                        $("#actualizar").html("Actualizar");
                    });
            } else {
                this.activarAcciones();
                $("#actualizar").html("Actualizar");
            }
        } else {
            this.activarAcciones();
            $("#actualizar").html("Actualizar");
              scrollToElement($(".is-invalid").parent());
        }
    }

    verificador() {
        let error = null;

        if (tinymce.activeEditor.initialized != true) {
            scrollToElement($("[name=cuerpo]").parent().parent());
            error = 'El cuerpo de la noticia no se ha cargado aún';

        } else if (!tinymce.activeEditor.getContent()) {
            scrollToElement($("[name=cuerpo]").parent().parent());
            error = 'El cuerpo de la noticia no puede quedar vacío';

        } else if (this.state.img == null) {
            scrollToElement($("#cargadorImagen"));
            error = 'No se ha agregado una imagen principal';

        } else if (this.state.crop.width <= 0 || this.state.crop.height <= 0) {
            scrollToElement($("#cargadorImagen"));
            error = 'No se ha seleccionado el área a mostrar';
        }

        if (error) {
            Toast.fire({
                type: 'error',
                title: error,
                timer: 6000
            });
            return true;
        } else {
            return false;
        }
    }

    procesarImagen(blob, success, failure) {
        const ref = this;

        try {
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', urlImgService + 'procesar.php');
            xhr.onload = function() {
                var json;

                if (xhr.status != 200) {
                    //Error HTTP
                    failure(xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    //Error en el formato del JSON
                    failure(xhr.responseText);
                    return;
                }
                success(json.location);
            };

            xhr.onerror = function(e) {
                failure();
            }
            var formData = new FormData();
            //Enviar unicamente el crop si este se hizo
            if (ref.state.crop.width > 0 && ref.state.crop.height > 0) {
                formData.append('crop', JSON.stringify(ref.state.crop));
            }
            formData.append('file', blob, "imagen." + ref.state.ext);
            xhr.send(formData);
        } catch (e) {
            failure();
        }
    }

    //Actualizar el crop
    onCropChange(crop, percentCrop) {
        //Estado de la primera carga   0:Sin cargar 1:Cargado 2:Modificado
        if (this.state.primeraCarga < 2) {
            this.setState({
                primeraCarga: this.state.primeraCarga + 1
            });
        }

        this.setState({
            crop,
            cargado: false
        });
    };
    //Callback despues de cargar la imagen en el cropper
    onImageLoaded(image) {
        //Estado de la primera carga   0:Sin cargar 1:Cargado 2:Modificado
        if (this.state.primeraCarga < 2) {
            this.setState({
                primeraCarga: this.state.primeraCarga + 1
            });
        }

        this.setState({
            img: image,
            //Reiniciar la area del crop
            crop: {
                ...this.state.crop,
                x: 0,
                y: 0,
                width: 0,
                height: 0
            }
        }, () => {
            if ($("#vizualizadorCropper").is(":hidden")) $("#vizualizadorCropper").show();
            $("#contenedorImg").animate({ opacity: 1 });
        });
        return false;
    };

    render() {
        return (
            <div className="container contenedorGeneral mt-5 mb-5" id="contenedorCrearNoticia">
                <h3 className="color-1">Editar noticia</h3>
                <hr></hr>
                <form>
                    <ContenidoNoticia
                        form={this.state.form}
                        onChange={this.handleChange}
                    />

                    <div id="typeaheadColaboradores" className="mb-3">
                        <label className="font-weight-bold">Colaboradores relacionados con la noticia</label>
                        <div className="row">
                            <div className="col-xl-12">
                                <Typeahead
                                    onChange={(seleccion) => this.setState({ colaboradoresSel: seleccion })}
                                    options={this.state.colaboradores}
                                    selected={this.state.colaboradoresSel}
                                    placeholder="Elige colaboradores..."
                                    emptyLabel="No se encontraron resultados"
                                    labelKey="nombre"
                                    multiple
                                    clearButton
                                    dropup={true}
                                    disabled={this.state.colaboradoresEstado}
                                    id="colaboradoresSel"
                                />
                            </div>
                        </div>
                    </div>

                    <CargadorImagen
                        onClick={this.onClickCargaImagen}
                        onChangeCargaImagenLocal={this.onChangeCargaImagenLocal}
                        onClickCargarImagenOnline={this.onClickCargarImagenOnline}
                        maximo={maximo}
                    />

                    <VizualizadorCropper
                        src={this.state.src}
                        crop={this.state.crop}
                        onCropChange={this.onCropChange}
                        onImageLoaded={this.onImageLoaded}
                        disabled={this.state.cropEstado}
                    />
                    <button id="actualizar" type="submit" className="btn btn-success" onClick={this.handleSubmit}>Actualizar</button>
                </form>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('editarNoticia')) {
        ReactDOM.render(<EditarNoticia />, document.getElementById('editarNoticia'));
        $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    }
});
