import React from 'react';
import Cropper from 'cropperjs';

const CargadorImagen = (props) => (
    <div id="cargadorImagen">
        <label className="font-weight-bold">Imagen principal</label>
        <small className="mb-2 form-text text-muted">El tamaño máximo permitido de la imagen es {props.maximo / 1e+6}MB</small>
        <div className="row">
            {/*
            <div className="form-group col-xl-6">
                <label>Cargar una imagen desde un enlace externo</label>
                <div className="input-group">
                    <input id="enlace" type="text" placeholder="" className="form-control" aria-describedby="btnEnlace" />
                    <div className="input-group-append" >
                        <button id="cargarEnlace" type="button" className="btn btn-primary" onClick={props.onClickCargarImagenOnline}>Cargar</button>
                    </div>
                </div>
            </div>
            */}
            <div className="form-group col-xl-12">
                <label>Cargar una imagen desde el equipo</label>
                <button id="buscarLocal" type="button" className="btn btn-primary d-block" onClick={props.onClick}>Buscar</button>
                <input type="file" accept="image/png, image/jpeg" onChange={props.onChangeCargaImagenLocal} />
            </div>
        </div>
    </div>
)
export default CargadorImagen
