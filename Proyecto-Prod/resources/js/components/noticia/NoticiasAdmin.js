import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Swal from 'sweetalert2'
import ListadoNoticias from './ListadoNoticias';

const axios = require('axios');
const crsf = $('meta[name=csrf-token]').attr("content");
const url = url_g;

const sleep = (milliseconds) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

export default class NoticiasAdmin extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="container mt-5 mb-5">
                <h3 className="color-1">Noticias</h3>
                <hr></hr>
                <ListadoNoticias />
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('noticiasAdmin')) {
        ReactDOM.render(< NoticiasAdmin />, document.getElementById('noticiasAdmin'));
        var listadoNoticias;
        $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });
        //Manejo de check - global
        $(".multi-check-cont, .multi-check-cont input[type=checkbox]").on("click", function(e) {
            var tableHead = $(this).closest("table")[0];
            var tableBody = $(this).closest(".dataTables_scrollHead").next(".dataTables_scrollBody").find("table")[0];
            var indiChecks = $(tableBody).find(".indi-check");
            var multiCheck = $(tableHead).find(".multi-check-cont input[type=checkbox]")[0];
            //Verificar que exista almenos un elemento en la tabla
            if (indiChecks.length > 0) {

                if ($(multiCheck).prop('checked')) {
                    $(multiCheck).prop('checked', false);
                    $(indiChecks).prop('checked', false);
                    //Deshabilitar opciones
                    $("#eliminar").prop("disabled", true);
                } else {
                    $(multiCheck).prop('checked', true);
                    $(indiChecks).prop('checked', true);
                    //Habilitar opciones
                    $("#eliminar").prop("disabled", false);
                }
            }
        });
        //Manejo de check - individual
        $(document).on('click', '#noticias tbody tr td:first-child', function(e) {
            e.preventDefault();
            var target;

            if (e.target.tagName == "LABEL") {
                target = $(e.target).parent().find(".indi-check")[0];
            } else if (e.target.tagName == "TD" || e.target.tagName == "DIV") {
                target = $(e.target).find(".indi-check")[0];

            }
            singleCheckHandler(target);
        });

        function singleCheckHandler(target) {
            var tableHead = $(target).closest(".dataTables_scrollBody").prev(".dataTables_scrollHead")[0];
            var tableBody = $(target).closest("table")[0];
            var multiCheck = $(tableHead).find(".multi-check-cont input[type=checkbox]")[0];
            //Con check, quitar check individual y global
            if (target.checked) {
                $(target).prop('checked', false);
                $(multiCheck).prop('checked', false);
                //Sin check, agregar check inidivual
            } else {
                $(target).prop('checked', true);
            }
            var totalChecks = $(tableBody).find(".indi-check").length;
            var checked = $(tableBody).find(".indi-check:checked").length;
            //Si todos los checks individuales estan activos, activar el global
            if (checked == totalChecks) {
                $(multiCheck).prop('checked', true);
            }
            //Si hay mas de un check individual, activar la opcion de eliminar
            if (checked > 0) {
                $("#eliminar").prop("disabled", false);
            } else {
                $("#eliminar").prop("disabled", true);
            }
        }

        listadoNoticias = $('#noticias').DataTable({
            "language": {
                "url": url + "libs/DataTables/language/Spanish.json"
            },
            "ajax": function(data, callback, settings) {
                //Mostrar carga si es la primera vez
                if ($("#listadoNoticias tbody tr td").not("td.dataTables_empty").parent().length == 0) {
                    $("#listadoNoticias .spinner-cont").css("visibility", "visible");
                }
                var res = axios.get(url + 'obtener_noticias_adm')
                    .then((res) => {
                        //Esperar antes de mostrar la lista
                        sleep(500)
                            .then(() => {
                                callback({ 'data': res.data });
                                //Mostrar header si se trata de la primera carga
                                if ($("#listadoNoticias .spinner-cont").css("visibility") == "visible") {
                                    //Ocultar carga y mostrar listado
                                    $("#listadoNoticias .spinner-cont").css("visibility", "hidden");
                                    $("#listadoNoticias").css("visibility", "visible");
                                }
                            });
                    })
                    .catch((e) => {
                    })
            },
            "order": [
                [1, 'asc']
            ],
            "responsive": false,
            "scrollY": true,
            "scrolX": true,
            "scrollCollapse": true,
            "paging": true,
            "columns": [
                {
                    "data": null
                },
                {
                    "data": "titulo"
                },
                {
                    "data": "fecha_publicacion"
                },
                {
                    "data": "fecha_actualizacion"
                },
                {
                    "data": null
                },
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "width": "5%",
                    "orderable": false,
                    "className": "text-center",
                    "render": function(data, type, row) {
                        return (
                            '<div class="custom-control custom-checkbox indi-check-cont">' +
                            '<input type="checkbox" class="custom-control-input indi-check" id="indi-check-' + data.id + '"/>' +
                            '<label class="custom-control-label ml-2" for="indi-check-' + data.id + '"></label>' +
                            '</div>'
                        );
                    },
                },
                {
                    "targets": 2,
                    "width": "5%",
                    "className": "text-center",
                    "render": function(data, type, row) {

                        const fecha = new Date(data);
                        return ('<div>' +
                            '<p>' + fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate() + '</p>' +
                            '<p>' + fecha.getHours() + "h " + fecha.getMinutes() + "m" + '</p>' +
                          '</div>');
                    }
                },
                {
                    "targets": 3,
                    "width": "5%",
                    "className": "text-center",
                    "render": function(data, type, row) {

                        const fecha = new Date(data);
                        return ('<div>' +
                            '<p>' + fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate() + '</p>' +
                            '<p>' + fecha.getHours() + "h " + fecha.getMinutes() + "m" + '</p>' +
                          '</div>');
                    }
                },
                {
                    "targets": 4,
                    "orderable": false,
                    "width": "5%",
                    "className": "text-center bg-white",
                    "render": function(data, type, row) {
                        return '<a data-tooltip="tooltip" data-placement="top" title="Editar" class="cont-icono editar btn btn-outline-info" href="noticia_editar/' + data.id + '"><i class="far fa-edit"></i></a>';
                    },
                },
                {
                    "targets": 5,
                    "data": null,
                    "width": "30%",
                    "visible": false,
                    "render": function(data, type, row) {
                        var colaboradores = "";
                        data.colaboradores.forEach(function(colab) {
                            colaboradores += colab.nombre + " ";
                        });
                        return colaboradores;
                    },
                }
            ],
            "preDrawCallback": function(settings) {
                $("#multi-check").prop('checked', false);
                $(".indi-check").prop('checked', false);
                $("#eliminar").prop("disabled", true);
            },
            "drawCallback": function(settings) {
                //Reinicializar los tooltip
                $('[data-tooltip="tooltip"]').tooltip({ trigger: "hover" });
            }
        });

        $("#eliminar").on("click", function(e) {
            Swal.fire({
                title: 'Confirmación',
                text: "¿Estas seguro de eliminar las noticias seleccionadas?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: (respuesta) => {

                    if (respuesta) {
                        var ids = [];
                        var data = listadoNoticias.rows(function(idx, data, node) {
                            return $(node).find(".indi-check")[0].checked == true ? true : false;
                        }).data();

                        for (var i = 0; i < data.length; i++) {
                            ids.push(data[i].id);
                        }
                        return axios.post(url + 'noticia_eliminar',
                            {
                                "ids": ids,
                            })
                            .then((res) => {
                                return res.data;
                            })
                            .catch((e) => {
                                return "Error";
                            });
                    }
                }
            })
                .then((data) => {

                    if (data.value == "Error") {
                        Toast.fire({
                            type: 'error',
                            title: 'Ha ocurrido un error, intente de nuevo en un momento',
                            timer: 6000
                        });
                    } else if (data.value == null) {
                    } else {
                        listadoNoticias.rows(function(idx, data, node) {
                            return $(node).find(".indi-check")[0].checked == true ? true : false;
                        }).remove().draw(false);
                        $("#eliminar").prop("disabled", true);
                        Toast.fire({
                            type: 'success',
                            title: 'Eliminado correctamente',
                        });
                    }
                })
                .catch((e) => {
                    Toast.fire({
                        type: 'error',
                        title: 'Ha ocurrido un error, intente de nuevo en un momento',
                        timer: 6000
                    });
                });
        });
    }
});
