import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import styled from 'styled-components';
import Swal from 'sweetalert2';

import 'react-circular-progressbar/dist/styles.css';
import { CircularProgressbar } from 'react-circular-progressbar';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
};

const thumb = {
    display: 'block',
    borderRadius: 2,
    width: 150,
    height: 150,
    boxSizing: 'border-box',
    margin: '3px'
};

const thumbInner = {
    display: 'none',
    minWidth: 0,
    width: '100%',
    height: '100%'
};

const img = {
    display: 'block',
    maxHeight: '100%',
    borderRadius: 5,
    border: '3px solid #eaeaea',
};

let primeraCarga = true;

export default class ImagesDragDropPreview extends Component {
    constructor(props) {
        super();
        this.onDrop = (aceptados, rechazados) => {
            //Bloquear acciones mientras se procesa
            props.setEstadoAccionesDragDropImg(true);
            $("#guardar, #actualizar").attr("disabled", true);
            //Restaurar color inicial
            $("#dragdrop").css('borderColor', 'gray');
            //Cancelar selecciones
            $("#prevs").find(".prevFrente.activo").removeClass("activo");
            this.setState({
                eliminarImg: true
            });
            //Verificar que no existan imagenes rechazadas
            if (rechazados.length == 0) {
                //Vericar que no exedan el maximo de imagenes
                if (aceptados.length + this.state.archivos.length <= props.imgMax) {
                    let archivosT = this.state.archivos;
                    aceptados.forEach((aceptado) => {
                        const archivoC = {
                            id: 0,
                            imagen: aceptado,
                            progreso: 0
                        }
                        archivosT.push(archivoC);
                    })
                    let rand;
                    let arrayT;
                    const max = props.imgMax;
                    const min = 1;
                    archivosT.forEach((archivoT, i) => {

                        if (archivoT.id == 0) {
                            //Generar id aleatorio
                            do {
                                rand = Math.floor(Math.random() * ((max - min) + 1)) + min;
                                arrayT = archivosT.find((archivoT, i) => {
                                    return archivoT.id == rand;
                                });
                            } while (arrayT != null);
                            //Asignar id generado
                            archivoT.id = rand;
                            //Generar objeto url unicamente si este no existe
                            if (archivoT.imagen.preview == null) {
                                archivoT.imagen.preview = URL.createObjectURL(archivoT.imagen);
                            }
                        }
                    });
                    primeraCarga = false;
                    this.setState({ archivos: archivosT }, () => {
                        //Mostrar imagenes
                        $("#contPrevImg").show();
                    })
                } else {
                    Toast.fire({
                        type: 'error',
                        title: 'El máximo de imágenes es 5',
                        timer: 6000
                    });
                }
            } else {
                let error = ""

                if (rechazados[0].size > props.opciones.maxSize) {
                    error = "Una o varias imágenes sobrepasan el tamaño máximo permitido (" + props.opciones.maxSize / 1e+6 + "mb)";
                } else {
                    error = "Una o varias imágenes no tienen un formato permitido (" + props.opciones.accept + ")";
                }
                Toast.fire({
                    type: 'error',
                    title: error,
                    timer: 8000
                });
            }
            //Desbloquear acciones
            props.setEstadoAccionesDragDropImg(false);
            $("#guardar, #actualizar").attr("disabled", false);
        };
        this.onDragEnter = (e) => {
            $("#dragdrop").css('borderColor', '#0275d8');
        }
        this.onDragLeave = (e) => {
            $("#dragdrop").css('borderColor', 'gray');
        }
        this.state = {
            archivos: []
        };
    }

    ajustarDimensiones(e) {
        const img = e.target;

        if (img.naturalWidth > img.naturalHeight) {
            img.style.width = '100%';
            img.style.height = 'auto';
        } else {
            img.style.width = 'auto';
            img.style.height = '100%';
        }
        //Mostrar imágenes
        if (!primeraCarga) {
            $("#contPrevImg")[0].scrollIntoView();
        }
        $(img).parent().show(300, () => {
        });
    }

    async componentDidUpdate() {

    }

    render() {
        const thumbs = this.state.archivos.map((archivo, i) => {
            return (
                <div onClick={(e) => { !this.props.imagenesOpts.selTodo ? this.props.onClick(e) : undefined }} className="prev" name="prev" style={thumb} key={archivo.id} data-key={archivo.id}>
                    <div className="img-prev" style={thumbInner}>
                        <img
                            onLoad={(e) => this.ajustarDimensiones(e)}
                            src={archivo.imagen.preview}
                            style={img}
                        />
                    </div>
                    <div className="prevFrente">
                        <div className="cont-opt ">
                            <div className="cont-opt-estado">
                                <div className="spinner-opt text-primary">
                                    <CircularProgressbar
                                        value={this.state.archivos.find(a => {
                                            return a.id === archivo.id;
                                        }).progreso}
                                        text={this.state.archivos.find(a => {
                                            return a.id === archivo.id;
                                        }).progreso + "%"}
                                        background={true}
                                        styles={{
                                            path: {
                                                stroke: `rgba(81, 196, 88, ${this.state.archivos.find(a => {
                                                    return a.id === archivo.id;
                                                }).progreso / 100})`,
                                                strokeLinecap: 'butt',
                                                transition: 'stroke-dashoffset 0.5s ease 0s',
                                            },
                                            trail: {
                                                stroke: '#d6d6d6',
                                            },
                                            text: {
                                                fill: 'white',
                                                fontSize: '30px',
                                                fontWeight: 'bold'
                                            },
                                            background: {
                                                fill: "#3E98C7"
                                            }
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return (
            <Dropzone
                onDrop={this.onDrop}
                onDragEnter={this.onDragEnter}
                onDragLeave={this.onDragLeave}
                accept={this.props.opciones.accept}
                disabled={this.props.opciones.disabled}
                maxSize={this.props.opciones.maxSize}
            >
                {({ getRootProps, getInputProps }) => (
                    <section>
                        <div id="dragdrop" {...getRootProps({ className: 'cursor-pointer mt-3' })}>
                            <input {...getInputProps()} />
                            <p className="text-center m-0 h5">Arrastre las imágenes a este cuadro, o haga click aquí para seleccionarlas</p>
                        </div>
                        <div id="contPrevImg" className="mt-3">
                            <label>Vista previa</label>
                            <div id="opcionesImagen" className="mb-1">
                                <button id="selTodoImg" onClick={this.props.onClick} type="button" data-toggle="tooltip" data-placement="top" title="Seleccionar todo" className="cont-icono btn btn-outline-info mr-2" disabled={this.props.imagenesOpts.selTodo}><i className="far fa-check-square"></i></button>
                                <button id="desTodoImg" onClick={this.props.onClick} type="button" data-toggle="tooltip" data-placement="top" title="Cancelar selección" className="cont-icono btn btn-outline-info mr-2" disabled={this.props.imagenesOpts.desTodo}><i className="far fa-square"></i></button>
                                <button id="eliminarImg" onClick={this.props.onClick} type="button" data-toggle="tooltip" data-placement="top" title="Eliminar selección" className="cont-icono btn btn-outline-danger" disabled={this.props.imagenesOpts.eliminar}><i className="far fa-trash-alt"></i></button>
                            </div>
                            <aside style={thumbsContainer} id="prevs">
                                {thumbs}
                            </aside>
                        </div>
                    </section>
                )}
            </Dropzone>
        );
    }
}
