import React, { Component } from 'react';
import ReactDOM from 'react-dom';
const url = url_g;
const axios = require('axios');

function ListaSocial(props) {
    return (
        <div className="social">
            {props.sociales.length > 0 ?
                props.sociales.map(social => {
                    switch (social.enlace.split('.')[1]) {
                        case "facebook":
                            return (
                                <a key={`social_${social.enlace}`} href={social.enlace} rel="noopener noreferrer" target="_blank">
                                    <i className="fab fa-facebook"> Facebook</i>
                                </a>
                            );
                            break;
                        case "twitter":
                            return (
                                <a key={`social_${social.enlace}`} href={social.enlace} rel="noopener noreferrer" target="_blank">
                                    <i className="fab fa-twitter"> Twitter</i>
                                </a>
                            );
                            break;
                        case "youtube":
                            return (
                                <a key={`social_${social.enlace}`} href={social.enlace} rel="noopener noreferrer" target="_blank">
                                    <i className="fab fa-youtube"> Youtube</i>
                                </a>

                            );
                            break;
                        case "instagram":
                            return (
                                <a key={`social_${social.enlace}`} href={social.enlace} rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-instagram"> Instagram</i>
                                </a>
                            );
                            break;
                        default:
                            return (
                                <a key={`social_${social.enlace}`} href={social.enlace} rel="noopener noreferrer" target="_blank">
                                    <i className="fa fa-link fleft"> {social.enlace}</i>
                                </a>
                            );
                            break;
                    }
                }) : <p>Sin redes sociales</p>
            }
        </div>
    );
}

function ListaValores(props) {
    return (
        <div className="valores">
            {props.valores.map(valor => {
                return <p key={`valor_${valor.valor}`}>{valor.valor}</p>
            }
            )}
        </div>
    );
}

function ListaTelefonos(props) {
    return (
        <div className="telefonos">
            {props.telefonos.length > 0 ?
                props.telefonos.map(telefono => {
                    return <div key={'t_' + telefono.numero}><p>(+506) {telefono.numero}</p></div>
                }) : <p>Sin teléfonos</p>
            }
        </div>
    );
}

export default class ColaboradorUsuario extends Component {

    constructor(props) {
        super(props)
        this.state = {
            nombre: "",
            direccion: "",
            descripcion: "",
            vision: "",
            mision: "",
            valores: [],
            telefonos: [],
            sociales: [],
            imagenes: []
        }
    }

    async componentDidMount() {

        try {
            const res = await axios.get(url + 'obtener_colaborador_usuario?id=' + id);
            this.setState({
                nombre: res.data.nombre,
                direccion: res.data.direccion,
                descripcion: res.data.descripcion,
                vision: res.data.vision,
                mision: res.data.mision,
                valores: res.data.valors,
                telefonos: res.data.telefonos,
                sociales: res.data.socials,
                imagenes: res.data.imagenColabs
            });
        } catch (e) {
        }
        window.scrollTo(0, 0);
        $(".container").css({ "opacity": "0", "display": "block" }).animate({ opacity: 1 }, () => {
            $("footer").animate({ opacity: 1 });
        });
    }

    handleImageLoad(e) {
        $(e.target).next().remove();
        $(e.target).css("visibility", "visible");
        $(e.target).animate({opacity: 1}, "fast");
    }

    render() {
        return (
            <div className="container mt-5 mb-1 display-none opacity-0">

                <div className="presentacion mb-4 text-center">
                    <h1 className="display-4 color-2"> {this.state.nombre} </h1>
                    <p className="lead"> {this.state.descripcion}  </p>
                </div>

                <div id="carrusel" className="carousel slide mb-3" data-ride="carousel">
                    <ol className="carousel-indicators">
                        {this.state.imagenes.map((imagen, i) => {
                            return (
                                <li key={'slide-' + i} data-target="#carrusel" data-slide-to={i} className={i == 0 ? "active" : undefined}></li>)
                        })}
                    </ol>
                    <div className="carousel-inner">
                        {this.state.imagenes.map((imagen, i) => {
                            return (
                                <div key={'carrusel-item-' + i} className={"carousel-item " + (i == 0 ? "active" : undefined)}>
                                    <div>
                                        <img className="d-block" src={imagen.url} onLoad={(e) => this.handleImageLoad(e)} />
                                        <div className="spinner-img-cont">
                                            <div className="spinner-border text-primary" role="status">
                                                <span className="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>)
                        })}
                    </div>
                    <a className="carousel-control-prev" href="#carrusel" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Anterior</span>
                    </a>
                    <a className="carousel-control-next" href="#carrusel" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Siguiente</span>
                    </a>
                </div >

                <div id="informacion" className="text-center">
                    <div className="container mt-5">
                        <div className="row">
                            <div className="col-sm-12 col-md-4 col-lg-4 mb-5">
                                <div className="feature-block">
                                    <h4 className="color-1">Misión</h4>
                                    <p> {this.state.mision}</p>
                                </div>
                            </div>

                            <div className="col-sm-12 col-md-4 col-lg-4 mb-5">
                                <div className="feature-block">
                                    <h4 className="color-1">Visión</h4>
                                    <p>{this.state.vision}</p>
                                </div>
                            </div>

                            <div className="col-sm-12 col-md-4 col-lg-4 mb-5">
                                <div className="feature-block">
                                    <h4 className="color-1">Valores</h4>
                                    <ListaValores valores={this.state.valores} />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <hr />
                        <div className="container">
                            <div className="section-title text-center">
                                <h2 className="color-2">Contacto</h2>
                            </div>
                        </div>
                        <hr />

                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12 col-md-4 col-lg-4 mb-5">
                                    <div className="feature-block">
                                        <div className="row icono mb-4">
                                            <i className="fa fa-map-marker m-auto"></i>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <p className="direccion">{this.state.direccion}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-md-4 col-lg-4 mb-5">
                                    <div className="feature-block">
                                        <div className="row icono mb-4">
                                            <i className="fa fa-phone m-auto"></i>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <ListaTelefonos telefonos={this.state.telefonos} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-md-4 col-lg-4 mb-5">
                                    <div className="feature-block">
                                        <div className="row icono mb-4">
                                            <i className="fas fa-users m-auto"></i>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <ListaSocial sociales={this.state.sociales} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('colaboradorUsuario')) {
        ReactDOM.render(<ColaboradorUsuario />, document.getElementById('colaboradorUsuario'));
    }
});
