import React from 'react';

const FormColaborador = (props) => (
    <form className="row">
        <div className="form-group col-xl-2 col-lg-3 col-md-3 col-sm-4">
            <label className="font-weight-bold" htmlFor="cedula">Cédula jurídica</label>
            <input type="text" onChange={props.onChange} value={props.form.cedula} className="form-control" minLength="10" maxLength="10" name="cedula"/>
            <div>
                <div className="invalid-feedback"></div>
                <small id="ayuda-cedula" className="form-text text-muted">Diez dígitos, sin cero al inicio ni guiones</small>
            </div>
        </div>
        <div className="form-group col-xl-10 col-lg-9 col-ms-12">
            <label htmlFor="nombre" className="font-weight-bold">Nombre</label>
            <input type="text" onChange={props.onChange} value={props.form.nombre} className="form-control" name="nombre" maxLength="100" />
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label htmlFor="direccion" className="font-weight-bold">Dirección</label>
            <textarea className="form-control" onChange={props.onChange} value={props.form.direccion} maxLength="500" name="direccion" rows="4" resize="false" ></textarea>
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label htmlFor="descripcion" className="font-weight-bold">Descripción</label>
            <textarea className="form-control" onChange={props.onChange} value={props.form.descripcion} maxLength="500" name="descripcion" rows="4" resize="false" ></textarea>
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label htmlFor="mision" className="font-weight-bold">Misión</label>
            <textarea className="form-control" onChange={props.onChange} value={props.form.mision} maxLength="300" name="mision" rows="3" resize="false" ></textarea>
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label htmlFor="vision" className="font-weight-bold">Visión</label>
            <textarea className="form-control" onChange={props.onChange} value={props.form.vision} maxLength="300" name="vision" rows="3" resize="false" ></textarea>
            <div className="invalid-feedback"></div>
        </div>
    </form>
)
export default FormColaborador
