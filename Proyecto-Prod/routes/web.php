<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Prevenir back history
Route::group(['middleware' => 'prevent-back-history'],function() {
  //Publico
  Route::get('/', function () {
      return view('principal');
  });
  //Noticias Usuario
  Route::get('/obtener_noticias_usuario', 'ControladorNoticia@getNoticiasUsuario');
  Route::get('/obtener_noticias_recientes/{cantidad}', 'ControladorNoticia@getNoticiasRecientes');
  Route::get('/obtener_noticia_usuario', 'ControladorNoticia@getNoticiaUsuario'); //informacon
  Route::get('/noticia_u/{id}', 'ControladorNoticia@irNoticiaUsuario'); //pagina
  //Colaborador Usuario
  //Colaboradores
  Route::get('/colaboradores', 'ControladorColaborador@irColaboradoresUsuario');
  Route::get('/obtener_colaboradores_usuario', 'ControladorColaborador@getColaboradoresUsuario');
  //Colaborador
  Route::get('/colaborador_u/{cedula}', 'ControladorColaborador@irColaboradorUsuario');
  Route::get('/obtener_colaborador_usuario', 'ControladorColaborador@getColaboradorUsuario');
  //Donacion Usuario
  Route::get('/donaciones', 'ControladorDonacion@irDonacionesUsuario');
  Route::get('/obtener_donacion_usuario', 'ControladorDonacion@getDonacionesUsuario');

  //Privado
  //Noticia Funciones
  Route::get('/noticia_crear', 'ControladorNoticia@create');
  Route::post('/noticia_guardar', 'ControladorNoticia@store');
  Route::get('/noticia_editar/{id}', 'ControladorNoticia@edit');
  Route::post('/noticia_actualizar', 'ControladorNoticia@update');
  Route::post('/noticia_eliminar', 'ControladorNoticia@destroy');
  //Noticias admin
  Route::get('/noticia', 'ControladorNoticia@index');
  Route::get('/obtener_noticias_adm', 'ControladorNoticia@getNoticiasAdm');
  Route::get('/obtener_noticia_adm', 'ControladorNoticia@getNoticiaAdm');
  //Colaborador funciones
  Route::get('/colaborador_crear', 'ControladorColaborador@create');
  Route::post('/colaborador_guardar', 'ControladorColaborador@store');
  Route::get('/colaborador_editar/{cedula}', 'ControladorColaborador@edit');
  Route::post('/colaborador_actualizar', 'ControladorColaborador@update');
  Route::post('/colaborador_eliminar', 'ControladorColaborador@destroy');
  //Colaborador admin
  Route::get('/colaborador', 'ControladorColaborador@index');
  Route::get('/obtener_colaboradores_adm', 'ControladorColaborador@getColaboradoresAdm');
  Route::get('/obtener_colaborador_adm', 'ControladorColaborador@getColaboradorAdm');
  //Administrador Funciones
  Route::get('/administrador_crear', 'ControladorAdministrador@create');
  Route::post('/administrador_guardar', 'ControladorAdministrador@store');
  Route::get('/administrador_editar/{correo}', 'ControladorAdministrador@edit');
  Route::post('/administrador_actualizar', 'ControladorAdministrador@update');
  Route::post('/administradores_desactivar', 'ControladorAdministrador@desactivarAdministradores');
  Route::post('/administradores_activar', 'ControladorAdministrador@activarAdministradores');
  Route::get('/obtener_administradores_activos', 'ControladorAdministrador@getAdministradoresActivos');
  Route::get('/obtener_administradores_inactivos', 'ControladorAdministrador@getAdministradoresInactivos');
  //Administrador admin
  Route::get('/administrador', 'ControladorAdministrador@index');
  Route::get('/obtener_administrador', 'ControladorAdministrador@getAdministrador');
  //Donacion Funciones
  Route::get('/donaciones_crear', 'ControladorDonacion@create');
  Route::post('/donacion_guardar', 'ControladorDonacion@store');
  Route::get('/donacion_editar/{numero_cuenta}', 'ControladorDonacion@edit');
  Route::post('/donacion_actualizar', 'ControladorDonacion@update');
  Route::post('/donaciones_eliminar', 'ControladorDonacion@destroy');
  //Donacion admin
  Route::get('/donacion', 'ControladorDonacion@index');
  Route::get('/obtener_donaciones_adm', 'ControladorDonacion@getDonacionesAdm');
  Route::get('/obtener_donacion_adm', 'ControladorDonacion@getDonacionAdm');
  //Autentificacion
  Auth::routes(['register' => false]); //Deshabilitar rutas de registro
  Auth::routes();
  Route::post('/autentificar', 'ControladorLogin@autentificar')->name("validar");
  Route::get('/logout', 'ControladorLogin@logout')->name("logout");
  Route::get('/administracion', 'ControladorLogin@login')->name('administracion');
});
