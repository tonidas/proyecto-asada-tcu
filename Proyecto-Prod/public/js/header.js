$(document).ready(function() {
    checkNavSticky();
    //Control del scroll en mobile
    $('.navbar-fostrap, #nav-back').click(function() {
        //Cerrar secciones
        $(".dropdown:visible").hide();
        //Mostrar navegacion
        $('.nav-fostrap').toggleClass('visible');
        //Mostrar fondo de navegacion
        $('#nav-back').toggleClass('cover-bg');
        //Cancelar scroll
        if ($('body').css("overflow-y") == "scroll") {
            $('html').addClass("overflow-hidden-i");
        } else {
            $('html').removeClass("overflow-hidden-i");
        }
    });
    //Funcionamiento de los drops en la version mobile
    $(".nav-fostrap li").click(function() {
        $width = $(window).width();

        if ($width < 900) {

            if ($(this).find(".dropdown:first").is(":hidden")) {
                $(".dropdown:visible").hide(150).fadeOut();
                $(this).find(".dropdown:first").show(150).fadeIn();
            } else {
                $(".dropdown:visible").hide(150).fadeOut();
            }
        }
    });
    //Control del sticky del nav web y mobile
    $(window).on("scroll", function() {
        checkNavSticky();
    })
});

function checkNavSticky() {
    var navbar_web = $(".nav-fostrap")[0];
    var sticky_web = navbar_web.offsetTop;
    var navbar_mobile = $(".nav-bg-fostrap")[0];
    var sticky_mobile = navbar_mobile.offsetTop;
    //Manejar sticky del nav web
    if (window.pageYOffset >= sticky_web) {
        navbar_web.classList.add("sticky")
    } else {
        navbar_web.classList.remove("sticky");
    }
    //Manejar sticky del nav mobile
    if (window.pageYOffset >= sticky_mobile) {
        navbar_mobile.classList.add("sticky")
    } else {
        navbar_mobile.classList.remove("sticky");
    }
}