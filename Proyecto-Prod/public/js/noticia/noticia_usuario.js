$(document).ready(function() {
    manejarBordes($(window).width());

    $(window).resize(function(e) {
        $width = $(window).width();
        manejarBordes($width);
    });
});

function manejarBordes($width) {

    if ($width < 576) { //XS
        $("#contenido, #panelD").addClass("r-bordes");
    } else if ($width < 768) { //SM
        $("#contenido, #panelD").addClass("r-bordes");
    } else if ($width < 992) { //MD
        $("#contenido, #panelD").addClass("r-bordes");
    } else if ($width < 1200) { //LG
        $("#contenido, #panelD").removeClass("r-bordes");
    } else { //XL
        $("#contenido, #panelD").removeClass("r-bordes");
    }
}