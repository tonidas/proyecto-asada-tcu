var letras = "A-Za-z";
var numeros = "0-9";
var espacios = "\\s";
var letrasAcentos = "áéíóúÁÉÍÓÚäëïöüÄËÏÖÜ";
var puntuaciones = ".,";
var simbolos = "!#$%&'*+/=?^_`{|}~-";
var correo = "(^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]{1,64}@)([a-zA-Z0-9-]{1,94}\\.)([a-zA-Z0-9-]{1,94}$)";

$(document).ready(function() {
    $(".controlaa").on("change, paste, keyup", function(e) {
        validar($(this).parent());
    });
})

function invalido(elemento, error, texto) {
    $(elemento).addClass("is-invalid");
    $(elemento).removeClass("is-valid");
    error.text(texto);
    error.show();
}

function valido(elemento, error) {
    $(elemento).addClass("is-valid");
    $(elemento).removeClass("is-invalid");
    error.text("");
    error.hide();
    return true;
}

function validar(form) {
    var control = $(form).find(".control");
    var resultado = false;
    var valor = "";
    var exp;

    $.each(control, function(o, elemento) {
        var error;

        if ($(elemento).parent().hasClass("input-group")) {
            error = $(elemento).parent().next(".invalid-feedback");
        } else {
            error = $(elemento).next(".invalid-feedback");
        }

        if (elemento.nodeName == "INPUT" || elemento.nodeName == "TEXTAREA") {
            valor = $(elemento).val().trim() || "";

            if ($(elemento).hasClass("minimo")) {
                var min = $(elemento).prop("min")

                if ($(elemento).val().length != min) {
                    return resultado = invalido(elemento, error, "El tamaño mínimo es " + min);
                }
            }

            if ($(elemento).hasClass("requerido")) {

                if (valor.length == 0) {
                    return resultado = invalido(elemento, error, "Este espacio es requerido");
                }
            }

            if ($(elemento).hasClass("valCorreo")) {
                exp = new RegExp(correo);

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "El correo no es valido");
                }
            }

            if ($(elemento).hasClass("valLetras")) {
                exp = new RegExp("^[" + letras + letrasAcentos + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan letras");
                }
            }

            if ($(elemento).hasClass("valLetrasEspacios")) {
                exp = new RegExp("^[" + letras + letrasAcentos + espacios + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan letras y espacios");
                }
            }

            if ($(elemento).hasClass("valLetrasNumeros")) {
                exp = new RegExp("^[" + letras + numeros + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan letras sin acentos y números");
                }
            }

            if ($(elemento).hasClass("valLetrasNumerosAcentos")) {
                exp = new RegExp("^[" + letras + letrasAcentos + numeros + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan letras y números");
                }
            }

            if ($(elemento).hasClass("valLetrasAcentosNumerosPuntuacionesSimbolos")) {
                exp = new RegExp("^[" + letras + letrasAcentos + numeros + puntuaciones + simbolos + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan letras, números y caracteres generales");
                }
            }

            if ($(elemento).hasClass("valLetrasAcentosNumerosEspacioPuntuacionesSimbolos")) {
                exp = new RegExp("^[" + letras + letrasAcentos + numeros + espacios + puntuaciones + simbolos + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan letras, números, espacios y caracteres generales");
                }
            }

            if ($(elemento).hasClass("valNumerosEnteros")) {
                exp = new RegExp("^[" + numeros + "]+$");

                if (valor.match(exp) == null) {
                    return resultado = invalido(elemento, error, "Solo se aceptan números enteros positivos");
                }
            }
        }

        if (elemento.nodeName == "SELECT") {
            valor = $(elemento).val() || "0";

            if ($(elemento).hasClass("requerido")) {

                if (valor == "0") {
                    return resultado = invalido(elemento, error, "Seleccione una opción");
                }
            }
        }
        valido(elemento, error);
    });
    return resultado;
}