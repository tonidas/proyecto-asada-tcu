<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $numero_cuenta
 * @property string $nombre_banco
 * @property string $nombre_propietario
 * @property string $moneda
 */
class Cuenta extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cuenta';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'numero_cuenta';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombre_banco', 'nombre_propietario', 'moneda'];

}
