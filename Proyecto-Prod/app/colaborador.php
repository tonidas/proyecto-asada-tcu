<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $cedula
 * @property string $nombre
 * @property string $direccion
 * @property string $descripcion
 * @property string $vision
 * @property string $mision
 * @property Noticia[] $noticias
 * @property ImagenColab[] $imagenColabs
 * @property Social[] $socials
 * @property Telefono[] $telefonos
 * @property Valor[] $valors
 */
class Colaborador extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'colaborador';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'direccion', 'descripcion', 'vision', 'mision'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function noticias()
    {
        return $this->belongsToMany('App\Noticia', 'colaborador_noticia', 'fk_id_colaborador', 'fk_id_noticia');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function imagenColabs()
    {
        return $this->hasMany('App\ImagenColab', 'fk_id_colaborador', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socials()
    {
        return $this->hasMany('App\Social', 'fk_id_colaborador', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function telefonos()
    {
        return $this->hasMany('App\Telefono', 'fk_id_colaborador', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function valors()
    {
        return $this->hasMany('App\Valor', 'fk_id_colaborador', 'id');
    }
}
