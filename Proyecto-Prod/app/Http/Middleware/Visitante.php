<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Visitante
{
    /**
     * Handle the incoming request.
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Verificar si esta logeado, de estarlo se redirecciona a noticia
        if (Auth::check()) {
            return Redirect("noticia");
        }
        //Continuar con el flujo normal si es un visitante
        return $next($request);
    }

}
