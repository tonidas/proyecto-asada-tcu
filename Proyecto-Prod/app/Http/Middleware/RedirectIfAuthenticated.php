<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
     public function handle($request, Closure $next, $guard = null)
     {
         $correo = $request->input('correo');
         $password = $request->input('password');

         if (Auth::guard('web')->attempt(['correo' => $correo, 'password' => $password, 'estado' => true])) {
             return redirect('noticia');
         }
         return $next($request);
     }
}
