<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerificarRol
{
    /**
     * Handle the incoming request.
     *
     * @param  string  $rol
     * @return mixed
     */
    public function handle($request, Closure $next, $rol)
    {
        if (! Auth::user()->hasRole($rol)) {
            return Redirect("/administracion");
        }
        return $next($request);
    }

}
