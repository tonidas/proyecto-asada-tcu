<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use App\Administrador;
use App\Rol;
use App\RolAdministrador;

class ControladorAdministrador extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('verificar.rol:Superusuario');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return view("administrador/administradores_superadm");
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view("administrador/crear_administrador");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $form = $request->input('form');
      //Validar formulario
      $atributos = [
          'cedula' => 'cédula',
          'nombreCompleto' => 'nombre completo',
          'contrasena' => 'contraseña',
          'confirmarContrasena' => 'confirmar contraseña',
          'telefono' => 'teléfono',
          'correo' => 'correo electrónico'
      ];
      $reglas = [
          'cedula' => 'required|min:9|max:9',
          'nombreCompleto' => 'required|max:100',
          'correo' => 'required|max:100|unique:administrador',
          'contrasena' => 'required|min:10|max:20|same:confirmarContrasena',
          'confirmarContrasena' => 'required|min:10|max:20',
          'telefono' => 'required|min:8|max:8'
      ];
      $mensajesError = [
          'contrasena.same' => 'Las contraseñas no coinciden'
      ];
      $validator = Validator::make($form, $reglas, $mensajesError);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      $id = DB::table('administrador')->insertGetId([
          'cedula' => $form['cedula'],
          'nombre_completo' => $form['nombreCompleto'],
          'correo' => $form['correo'],
          'password' => Hash::make($form['contrasena']),
          'telefono' => $form['telefono'],
          'estado' => true,
      ]);
      DB::table('rol_administrador')->insert([
          'fk_id_administrador' => $id,
          'fk_id_rol' => Rol::select("id")->where("nombre", "=", "Administrador")->first()->id
      ]);
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response($e->getMessage(), 500);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      return view("administrador/editar_administrador", ["id" => $id]);
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    try {
      $id = $request->input('id');
      $form = $request->input('form');
      //Validar formulario
      $atributos = [
          'cedula' => 'cédula',
          'nombreCompleto' => 'nombre completo',
          'contrasena' => 'contraseña',
          'confirmarContrasena' => 'confirmar contraseña',
          'telefono' => 'teléfono',
          'correo' => 'correo electrónico'
      ];
      $reglas;
      $reglas['cedula'] = 'required|min:9|max:9';
      $reglas['nombreCompleto'] = 'required|max:100';
      $reglas['correo'] = 'required|max:100|unique:administrador,correo,' . Administrador::select('correo')->where('id', '=', $id)->first()->correo . ',correo';
      //Agregar reglas de contraseña si esta se va a cambiar
      if ($form['contrasena'] != "" && $form['confirmarContrasena'] != "") {
        $reglas['contrasena'] = 'required|min:10|max:20|same:confirmarContrasena';
        $reglas['confirmarContrasena'] = 'required|min:10|max:20';
      }
      $reglas['telefono'] = 'required|min:8|max:8';

      $mensajesError = [
          'contrasena.same' => 'Las contraseñas no coinciden'
      ];
      $validator = Validator::make($form, $reglas, $mensajesError);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      DB::table('administrador')
      ->where('id', '=', $id)
      ->update([
          'cedula' => $form['cedula'],
          'nombre_completo' => $form['nombreCompleto'],
          'correo' => $form['correo'],
          'telefono' => $form['telefono']
      ]);
      //Actualizar contraseña unicamente si esta se ha cambiado
      if ($form['contrasena'] != "" && $form['confirmarContrasena'] != "") {
        DB::table('administrador')
        ->where('id', '=', $id)
        ->update([
            'password' => Hash::make($form['contrasena'])
        ]);
      }
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function desactivarAdministradores(Request $response)
  {
      try {
        $ids = $response->input("ids");
        DB::table("administrador")
        ->whereIn('id', $ids)
        ->update(['estado' => 0]);
        return Response("OK", 200);
      } catch(\Exception $e) {
        return Response("Error", 500);
      }
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function activarAdministradores(Request $response)
  {
      try {
        $ids = $response->input("ids");
        DB::table("administrador")
        ->whereIn('id', $ids)
        ->update(['estado' => 1]);
        return Response("OK", 200);
      } catch(\Exception $e) {
        return Response("Error", 500);
      }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getAdministrador(Request $request)
  {
    try {
      $id = $request->input("id");
      $administrador = Administrador::select('correo', 'cedula', 'nombre_completo', 'telefono')
      ->where('id', "=", $id)
      ->first();
      return Response(['form' => $administrador], 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getAdministradoresActivos()
  {
    try {
      $administradores = Administrador::select('administrador.id', 'cedula', 'correo', 'nombre_completo', 'telefono')
      ->where([
        ['administrador.estado', "=", true]
      ])
      ->join('rol_administrador', 'administrador.id', '=', 'rol_administrador.fk_id_administrador')
      ->join('rol', 'rol_administrador.fk_id_rol', '=', 'rol.id')
      ->get();

      foreach($administradores as $administrador) {
          $superUsuario = RolAdministrador::select('rol.nombre')
          ->where([
            ['rol_administrador.fk_id_administrador', "=", $administrador->id],
            ['rol.nombre', "=", "Superusuario"]
          ])
          ->join('rol', 'rol_administrador.fk_id_rol', '=', 'rol.id')
          ->first();
          //Verificar si es superadm
          if ($superUsuario != null) {
              $administrador->isSuperUsuario = true;
          } else {
              $administrador->isSuperUsuario = false;
          }
      }

      return Response($administradores, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getAdministradoresInactivos()
  {
    try {
      $administradores = Administrador::select('administrador.id', 'cedula', 'correo', 'nombre_completo', 'telefono')
      ->where([
        ['rol.nombre', "!=", "Superusuario"],
        ['administrador.estado', "=", false]
      ])
      ->join('rol_administrador', 'administrador.id', '=', 'rol_administrador.fk_id_administrador')
      ->join('rol', 'rol_administrador.fk_id_rol', '=', 'rol.id')
      ->get();
      return Response($administradores, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }
}
