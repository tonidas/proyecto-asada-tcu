<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use App\Cuenta;

class ControladorDonacion extends Controller
{
  public function __construct()
  {
    $this->middleware('auth', ['except' => ['irDonacionesUsuario', 'getDonacionesUsuario']]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view("donacion/donaciones_admin");
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function irDonacionesUsuario()
  {
    return view("donacion/donaciones_usuario");
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view("donacion/crear_donacion");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $form = $request->input('form');
      //Validar formulario
      $atributos = [
          'numero_cuenta' => 'numero de cuenta',
          'nombre_banco' => 'nombre de banco',
          'nombre_propietario' => 'nombre de propietario',
          'moneda' => 'moneda'
      ];
      $reglas = [
          'numero_cuenta' => 'required|min:20|max:20',
          'nombre_banco' => 'required|max:100',
          'nombre_propietario' => 'required|max:100',
          'moneda' => 'required|max:100'
      ];
      $validator = Validator::make($form, $reglas);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      DB::table('cuenta')->insert([
          'numero_cuenta' => $form['numero_cuenta'],
          'nombre_banco' => $form['nombre_banco'],
          'nombre_propietario' => $form['nombre_propietario'],
          'moneda' => $form['moneda'],
      ]);
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  String  $numero_cuenta
   * @return \Illuminate\Http\Response
   */
  public function edit($numero_cuenta)
  {
      return view("/donacion/editar_donacion", ["numero_cuenta" => $numero_cuenta]);
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    try {
      $numero_cuenta = $request->input('numero_cuenta');
      $form = $request->input('form');
      //Validar formulario
      $atributos = [
        'numero_cuenta' => 'numero de cuenta',
        'nombre_banco' => 'nombre de banco',
        'nombre_propietario' => 'nombre de propietario',
        'moneda' => 'moneda'
      ];
      $reglas = [
        'numero_cuenta' => 'required|min:20|max:20',
        'nombre_banco' => 'required|max:100',
        'nombre_propietario' => 'required|max:100',
        'moneda' => 'required|max:100'
      ];

      $validator = Validator::make($form, $reglas);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      DB::table('cuenta')
      ->where('numero_cuenta', '=', $numero_cuenta)
      ->update([
          'nombre_banco' => $form['nombre_banco'],
          'nombre_propietario' => $form['nombre_propietario'],
          'moneda' => $form['moneda']
      ]);
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\donacion  $donacion
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $response)
  {
      try {
        $numero_cuentas = $response->input("numero_cuentas");
        Cuenta::destroy($numero_cuentas);
        return Response("OK", 200);
      } catch(\Exception $e) {
        return Response("Error", 500);
      }
  }

  /**
   * @return \Illuminate\Http\Response
   */
  public function getDonacionesAdm()
  {
    try {
      $donaciones = Cuenta::select(	'numero_cuenta','nombre_banco','nombre_propietario','moneda')->get();

      return Response($donaciones, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * @param  int  $numero_cuenta
   * @return \Illuminate\Http\Response
   */
  public function getDonacionAdm(Request $request)
  {
    try {
      $numero_cuenta = $request->input("numero_cuenta");
      $cuenta = Cuenta::select('numero_cuenta','nombre_banco','nombre_propietario','moneda')
      ->where('numero_cuenta', "=", $numero_cuenta)
      ->first();

      return Response(['form' => $cuenta], 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getDonacionesUsuario()
  {
    try {
      $donaciones = Cuenta::select(	'numero_cuenta','nombre_banco','nombre_propietario','moneda')->paginate(2);

      return Response($donaciones, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }
}
