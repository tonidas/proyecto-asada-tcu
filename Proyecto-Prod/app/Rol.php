<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property Administrador[] $administradors
 */
class Rol extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rol';

    /**
     * @var array
     */
    protected $fillable = ['nombre'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function administradors()
    {
        return $this->belongsToMany('App\Administrador', 'rol_administrador', 'fk_id_rol', 'fk_id_administrador');
    }
}
