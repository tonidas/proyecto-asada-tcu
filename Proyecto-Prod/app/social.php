<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $fk_id_colaborador
 * @property string $enlace
 * @property Colaborador $colaborador
 */
class Social extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'social';

    /**
     * @var array
     */
    protected $fillable = ['fk_id_colaborador', 'enlace'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function colaborador()
    {
        return $this->belongsTo('App\Colaborador', 'fk_id_colaborador', 'id');
    }
}
