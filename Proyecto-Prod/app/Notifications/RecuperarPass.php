<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class RecuperarPass extends Notification
{

    public function __construct($token)
    {
        $this->token = $token;
    }


    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $data = [
            'nombre' => $notifiable->nombre_completo,
            'url' => url('password/reset', $this->token)
        ];

        return (new MailMessage)
            ->subject("Recuperación de contraseña")
            ->markdown('auth.passwords.plantilla_recuperar_pass', ['data' => $data]);
    }
}
