<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fk_id_noticia
 * @property string $fk_id_colaborador
 * @property Colaborador $colaborador
 * @property Noticia $noticium
 */
class ColaboradorNoticia extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'colaborador_noticia';

    /**
     * @var array
     */
    protected $fillable = ['fk_id_noticia', 'fk_id_colaborador'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function colaborador()
    {
        return $this->belongsTo('App\Colaborador', 'fk_id_colaborador', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function noticium()
    {
        return $this->belongsTo('App\Noticia', 'fk_id_noticia');
    }
}
