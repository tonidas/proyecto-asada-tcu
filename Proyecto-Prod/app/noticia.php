<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $fk_id_administrador
 * @property string $titulo
 * @property string $descripcion
 * @property string $cuerpo
 * @property string $img_principal
 * @property string $fecha_publicacion
 * @property string $fecha_actualizacion
 * @property Administrador $administrador
 * @property Colaborador[] $colaboradors
 */
class Noticia extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'noticia';

    /**
     * @var array
     */
    protected $fillable = ['fk_id_administrador', 'titulo', 'descripcion', 'cuerpo', 'img_principal', 'fecha_publicacion', 'fecha_actualizacion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administrador()
    {
        return $this->belongsTo('App\Administrador', 'fk_id_administrador');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function colaboradors()
    {
        return $this->belongsToMany('App\Colaborador', 'colaborador_noticia', 'fk_id_noticia', 'fk_id_colaborador');
    }
}
