<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TablaRolAdministradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol_administrador')->insert([
            'fk_id_administrador' => '1',
            'fk_id_rol' => '1'
        ]);
        DB::table('rol_administrador')->insert([
            'fk_id_administrador' => '2',
            'fk_id_rol' => '2'
        ]);
    }
}
