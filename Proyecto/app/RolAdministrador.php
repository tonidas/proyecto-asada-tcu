<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fk_id_administrador
 * @property int $fk_id_rol
 * @property Administrador $administrador
 * @property Rol $rol
 */
class RolAdministrador extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'rol_administrador';

    /**
     * @var array
     */
    protected $fillable = ['fk_id_administrador', 'fk_id_rol'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administrador()
    {
        return $this->belongsTo('App\Administrador', 'fk_id_administrador');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rol()
    {
        return $this->belongsTo('App\Rol', 'fk_id_rol');
    }
}
