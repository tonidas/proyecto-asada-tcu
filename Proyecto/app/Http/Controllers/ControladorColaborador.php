<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use App\Colaborador;
use App\Valor;
use App\Telefono;
use App\Social;
use App\ImagenColab;

class ControladorColaborador extends Controller
{

  public function __construct()
  {
    $this->middleware('auth', ['except' => ['getColaboradoresUsuario', 'getColaboradorUsuario', 'irColaboradorUsuario', 'irColaboradoresUsuario']]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view("colaborador/colaboradores_admin");
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view("colaborador/crear_colaborador");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $form = $request->input('form');
      $valores = $request->input('valores');
      $telefonos = $request->input('telefonos');
      $redes = $request->input('redes');
      $imagenes = $request->input('imagenes');
      //Validar formulario
      $atributos = [
          'cedula' => 'cédula',
          'direccion' => 'dirección',
          'descripcion' => 'descripción',
          'mision' => 'misión',
          'vision' => 'visión'
      ];
      $reglas = [
          'cedula' => 'required|unique:colaborador,cedula|max:10',
          'nombre' => 'required|unique:colaborador,nombre|max:100',
          'direccion' => 'required|max:500',
          'descripcion' => 'required|max:500',
          'mision' => 'required|max:300',
          'vision' => 'required|max:300'
      ];
      $validator = Validator::make($form, $reglas);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      $id = DB::table('colaborador')->insertGetId([
          'cedula' => $form['cedula'],
          'nombre' => $form['nombre'],
          'direccion' => $form['direccion'],
          'descripcion' => $form['descripcion'],
          'mision' => $form['mision'],
          'vision' => $form['vision'],
      ]);

      foreach($valores as $valor) {
        $temp = new Valor;
        $temp->valor = $valor['valor'];
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      foreach($telefonos as $telefono) {
        $temp = new Telefono;
        $temp->numero = $telefono['numero'];
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      foreach($redes as $red) {
        $temp = new Social;
        $temp->enlace = $red['enlace'];
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      foreach($imagenes as $imagen) {
        $temp = new ImagenColab;
        $temp->url = $imagen;
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      return view("colaborador/editar_colaborador", ["id" => $id]);
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    try {
      $id = $request->input('id');
      $form = $request->input('form');
      $valores = $request->input('valores');
      $telefonos = $request->input('telefonos');
      $redes = $request->input('redes');
      $imagenes = $request->input('imagenes');
      //Validar formulario
      $atributos = [
          'direccion' => 'dirección',
          'descripcion' => 'descripción',
          'mision' => 'misión',
          'vision' => 'visión'
      ];
      $reglas = [
          //unique:connection.tabla,columna,excepcion,nombreColumnaExcepcion(de ser diferente a 'id')
          'nombre' => 'required|unique:colaborador,nombre,' . Colaborador::select('id')->where('id', '=', $id)->first()->id . ',id|max:100',
          'direccion' => 'required|max:500',
          'descripcion' => 'required|max:500',
          'mision' => 'required|max:300',
          'vision' => 'required|max:300'
      ];
      $validator = Validator::make($form, $reglas);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      DB::table('colaborador')
      ->where('id', '=', $id)
      ->update([
          'cedula' => $form['cedula'],
          'nombre' => $form['nombre'],
          'direccion' => $form['direccion'],
          'descripcion' => $form['descripcion'],
          'mision' => $form['mision'],
          'vision' => $form['vision'],
      ]);

      DB::table('valor')
      ->where('fk_id_colaborador', "=", $id)
      ->delete();
      DB::table('telefono')
      ->where('fk_id_colaborador', "=", $id)
      ->delete();
      DB::table('social')
      ->where('fk_id_colaborador', "=", $id)
      ->delete();
      DB::table('imagen_colab')
      ->where('fk_id_colaborador', "=", $id)
      ->delete();

      foreach($valores as $valor) {
        $temp = new Valor;
        $temp->valor = $valor['valor'];
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }

      foreach($telefonos as $telefono) {
        $temp = new Telefono;
        $temp->numero = $telefono['numero'];
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      foreach($redes as $red) {
        $temp = new Social;
        $temp->enlace = $red['enlace'];
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      foreach($imagenes as $imagen) {
        $temp = new ImagenColab;
        $temp->url = $imagen;
        $temp->fk_id_colaborador = $id;
        $temp->save();
      }
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $response)
  {
      try {
        $ids = $response->input("ids");

        DB::table("valor")
        ->whereIn("fk_id_colaborador", $ids)
        ->delete();
        DB::table("telefono")
        ->whereIn("fk_id_colaborador", $ids)
        ->delete();
        DB::table("social")
        ->whereIn("fk_id_colaborador", $ids)
        ->delete();
        DB::table("imagen_colab")
        ->whereIn("fk_id_colaborador", $ids)
        ->delete();
        DB::table("colaborador_noticia")
        ->whereIn("fk_id_colaborador", $ids)
        ->delete();
        Colaborador::destroy($ids);
        return Response("OK", 200);
      } catch(\Exception $e) {
        return Response("Error", 500);
      }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getColaboradoresAdm()
  {
    try {
      $colaboradores = DB::table('colaborador')
      ->select('id', 'cedula', 'nombre')
      ->get();
      return Response($colaboradores, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getColaboradorAdm(Request $request)
  {
    try {
      $id = $request->input("id");
      $colaborador = Colaborador::where('id', "=", $id)

      ->first();
      $valores = Valor::select('valor')
      ->where('fk_id_colaborador', "=", $id)
      ->get();
      $telefonos = Telefono::select('numero')
      ->where('fk_id_colaborador', "=", $id)
      ->get();
      $redes = Social::select('id', 'enlace')
      ->where('fk_id_colaborador', "=", $id)
      ->get();
      $imagenes = ImagenColab::select('id', 'url')
      ->where('fk_id_colaborador', "=", $id)
      ->get();
      return Response([
        'form' => $colaborador,
        'valores' => $valores,
        'telefonos' => $telefonos,
        'redes' => $redes,
        'imagenes' => $imagenes
      ], 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getColaboradoresUsuario(Request $request)
  {
    try {
      $totalPagina = $request->input('totalPagina', 6);
      $paginaActual = $request->input('paginaActual', 1);

      $colaboradores = DB::table('colaborador')
      ->select('id', 'cedula', 'nombre','descripcion')
      //->orderBy('fecha_actualizacion', 'desc')
      //Total de elementos por fragmento
      ->limit($totalPagina)
      //offset especifica desde donde se inicia el corte
      //ignorar las primeras 3 filas
      ->offset(($paginaActual - 1) * $totalPagina)
      ->get();
      $total = Colaborador::count();

      foreach ($colaboradores as $colaborador) {
        $imagenesColaborador = ImagenColab::select('url')
        ->where("fk_id_colaborador", "=", $colaborador->id)
        ->get();;
        //Obtener dimensiones de cada imagen
        foreach($imagenesColaborador as $imagenColaborador) {
            $imagenColaborador->dimensiones = getimagesize($imagenColaborador->url);
        }
        $colaborador->imgColaborador = $imagenesColaborador;
      }
      return Response([
        "totalPagina" => $totalPagina,
        "paginaActual" => $paginaActual,
        "colaboradores" => $colaboradores,
        "total" => $total
      ], 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function irColaboradoresUsuario()
  {
      return view("colaborador/colaboradores_usuarios");
  }

  /**
   * @param  int  $cedula
   * @return \Illuminate\Http\Response
   */
  public function irColaboradorUsuario($id)
  {
      $colaborador = Colaborador::select('nombre')
      ->where('id', "=", $id)
      ->first();

      if ($colaborador) {
          return view("colaborador/colaborador_usuario", ["id" => $id]);
      } else {
          return redirect('colaboradores');
      }
  }

  /**
  *
  * @return \Illuminate\Http\Response
  */
  public function getColaboradorUsuario(Request $request)
  {
      try {
          $id = $request->input("id");
          $colaborador = Colaborador::select('nombre', 'direccion', 'descripcion', 'vision', 'mision')
          ->where('id', "=", $id)
          ->first();

          $imagenes = ImagenColab::select('url')
          ->where("fk_id_colaborador", "=", $id)
          ->get();

          $social = Social::select('enlace')
          ->where("fk_id_colaborador", "=", $id)
          ->get();

          $telefono = Telefono::select('numero')
          ->where("fk_id_colaborador", "=", $id)
          ->get();

          $valor = Valor::select('valor')
          ->where("fk_id_colaborador", "=", $id)
          ->get();

          if ($colaborador) {
              $colaborador->imagenColabs = $imagenes;
              $colaborador->socials = $social;
              $colaborador->telefonos = $telefono;
              $colaborador->valors = $valor;
          }
          return Response($colaborador, 200);
      } catch(\Exception $e) {
          return Response("Error", 500);
      }
  }
}
