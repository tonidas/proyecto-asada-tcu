<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class ControladorLogin extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('visitante');

          $this->middleware('visitante', ['except' => ['logout', 'loggedOut']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('administracion/login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function autentificar(Request $request)
    {
        $correo = $request->input('correo');
        $password = $request->input('password');
        //Validar formulario
        $atributos = [
            'password' => 'contraseña',
            'correo' => 'correo electrónico'
        ];
        $reglas = [
            'password' => 'required|min:10|max:20',
            'correo' => 'required|max:100',
        ];
        $validator = Validator::make($request->all(), $reglas);
        $validator->setAttributeNames($atributos);

        if ($validator->fails()) {
            return Response(['erroresForm' => $validator->errors()], 500);
        }
        //Verificar credenciales
        $admin = DB::table('administrador')
        ->where([
          ['correo', '=', $correo]
        ])
        ->first();

        if ($admin && Hash::check($password, $admin->password)) {
            //Verificar estado de la cuenta
            if (Auth::guard('web')->attempt(['correo' => $correo, 'password' => $password, 'estado' => true])) {
                return Response("OK", 200);

            } else {

              return Response(['erroresForm' => [
                'correo' => [
                  '0' => 'Esta cuenta se encuentra suspendida, contacte al administrador.'
                ],
                'password' => [
                  '0' => ' '
                ]
              ]], 500);
            }

        } else {

            return Response(['erroresForm' => [
              'correo' => [
                '0' => 'El correo o contraseña ingresados son incorrectos.'
              ],
              'password' => [
                '0' => ' '
              ]
            ]], 500);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/administracion');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
