<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use App\Noticia;
use App\Colaborador;
use App\ColaboradorNoticia;
use Carbon\Carbon;

class ControladorNoticia extends Controller
{

  public function __construct()
  {
    $this->middleware('auth', ['except' => ['getNoticiasUsuario', 'getNoticiasRecientes', 'irNoticiaUsuario', 'getNoticiaUsuario']]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view("noticia/noticias_admin");
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view("noticia/crear_noticia");
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //Cambiar correo al cargado en sesion al al iniciar sesion
    try {
      $imagen = $request->input('imagen');
      $form = $request->input('form');
      $cuerpo = $request->input('cuerpo');
      $colaboradores = $request->input('colaboradores');
      //Validar formulario
      $atributos = [
          'titulo' => 'título',
          'descripcion' => 'descripción'
      ];
      $reglas = [
          'titulo' => 'required|unique:noticia,titulo|max:200',
          'descripcion' => 'required|max:500'
      ];
      $validator = Validator::make($form, $reglas);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      $id = DB::table('noticia')->insertGetId([
          'fk_id_administrador' => Auth::user()->id,
          'img_principal' => $imagen,
          'titulo' => $form['titulo'],
          'descripcion' => $form['descripcion'],
          'cuerpo' => $cuerpo,
          'fecha_publicacion' => date('Y-m-d H:i:s'),
          'fecha_actualizacion' => date('Y-m-d H:i:s')
      ]);
      $colaboradoresRel = array();

      foreach($colaboradores as $colaborador) {
        $colaboradorRel = new ColaboradorNoticia;
        $colaboradorRel->fk_id_noticia = $id;
        $colaboradorRel->fk_id_colaborador = $colaborador['id'];
        $colaboradorRel->save();
      }
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      return view("noticia/editar_noticia", ["id" => $id]);
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    try {
      $id = $request->input("id");
      $imagen = $request->input('imagen');
      $form = $request->input('form');
      $cuerpo = $request->input('cuerpo');
      $colaboradores = $request->input('colaboradores');
      //Validar formulario
      $atributos = [
          'titulo' => 'título',
          'descripcion' => 'descripción'
      ];
      $reglas = [
          'titulo' => 'required|unique:noticia,titulo,' . Noticia::select('titulo')->where('id', '=', $id)->first()->titulo . ',titulo|max:200',
          'descripcion' => 'required|max:500'
      ];
      $validator = Validator::make($form, $reglas);
      $validator->setAttributeNames($atributos);

      if ($validator->fails()) {
          return Response(['erroresForm' => $validator->errors()], 500);
      }
      //Procesos con DB
      DB::table('noticia')
      ->where('id', '=', $id)
      ->update([
        'img_principal' => $imagen,
        'titulo' => $form['titulo'],
        'descripcion' => $form['descripcion'],
        'cuerpo' => $cuerpo,
        'fecha_actualizacion' => date('Y-m-d H:i:s')
      ]);

      DB::table('colaborador_noticia')
      ->where('fk_id_noticia', "=", $id)
      ->delete();
      $colaboradoresRel = array();

      foreach($colaboradores as $colaborador) {
        $colaboradorRel = new ColaboradorNoticia;
        $colaboradorRel->fk_id_noticia = $id;
        $colaboradorRel->fk_id_colaborador = $colaborador['id'];
        $colaboradorRel->save();
      }
      return Response("OK", 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $response)
  {
      try {
        $ids = $response->input("ids");
        DB::table("colaborador_noticia")
        ->whereIn("fk_id_noticia", $ids)
        ->delete();
        Noticia::destroy($ids);
        return Response("OK", 200);
      } catch(\Exception $e) {
        return Response("Error", 500);
      }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getNoticiasAdm()
  {
    try {
      $noticias = Noticia::select('id', 'titulo', 'fecha_publicacion', 'fecha_actualizacion')->get();

      foreach ($noticias as $noticia) {
        $colabNot = ColaboradorNoticia::select('fk_id_colaborador as id')
        ->where("fk_id_noticia", "=", $noticia->id)
        ->get();

        $colaboradores = DB::table("colaborador")
        ->select("nombre")
        ->whereIn('id', $colabNot)
        ->get();
        $noticia->colaboradores = $colaboradores;
        $noticia->fecha_publicacion_dif = Carbon::parse($noticia->fecha_publicacion)->diffForHumans();
        $noticia->fecha_actualizacion_dif = Carbon::parse($noticia->fecha_actualizacion)->diffForHumans();
      }
      return Response($noticias, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getNoticiaAdm(Request $request)
  {
    try {
      $id = $request->input("id");
      $noticia = Noticia::select('titulo', 'descripcion', 'img_principal', 'cuerpo')
      ->where('id', "=", $id)
      ->first();

      $colaboradores = ColaboradorNoticia::select('id', 'nombre')
      ->where("colaborador_noticia.fk_id_noticia", "=", $id)
      ->join("colaborador", "colaborador_noticia.fk_id_colaborador", "=", "colaborador.id")
      ->get();

      $noticia->colaboradores = $colaboradores;
      return Response($noticia, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getNoticiasUsuario(Request $request)
  {
    try {
      $totalPagina = $request->input('totalPagina', 6);
      $paginaActual = $request->input('paginaActual', 1);
      $noticias = DB::table('noticia')
      ->select('id', 'titulo', 'descripcion', 'fecha_publicacion', 'fecha_actualizacion', 'img_principal')
      ->orderBy('fecha_actualizacion', 'desc')
      //Total de elementos por fragmento
      ->limit($totalPagina)
      //offset especifica desde donde se inicia el corte
      //ignorar las primeras 3 filas
      ->offset( ((($paginaActual - 1) * 3) + (3 * $paginaActual)) )
      ->get();
      //descontar del total las filas ignoradas
      $total = Noticia::count() - 3;

      foreach ($noticias as $noticia) {
          $noticia->fecha_publicacion_dif = Carbon::parse($noticia->fecha_publicacion)->diffForHumans();
          $noticia->fecha_actualizacion_dif = Carbon::parse($noticia->fecha_actualizacion)->diffForHumans();
      }

      return Response([
        "totalPagina" => $totalPagina,
        "paginaActual" => $paginaActual,
        "noticias" => $noticias,
        "total" => $total
      ], 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getNoticiasRecientes($cantidad)
  {
    try {
      $noticias = Noticia::select('id', 'titulo', 'descripcion', 'fecha_publicacion', 'fecha_actualizacion', 'img_principal')
      ->orderBy('fecha_actualizacion', 'desc')
      ->take($cantidad)
      ->get();

      foreach ($noticias as $noticia) {
          $noticia->fecha_publicacion_dif = Carbon::parse($noticia->fecha_publicacion)->diffForHumans();
          $noticia->fecha_actualizacion_dif = Carbon::parse($noticia->fecha_actualizacion)->diffForHumans();
      }
      return Response($noticias, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   *
   * @return \Illuminate\Http\Response
   */
  public function getNoticiaUsuario(Request $request)
  {
    try {
      $id = $request->input("id");

      $noticia = Noticia::select('id','titulo', 'descripcion', 'fecha_publicacion', 'fecha_actualizacion', 'img_principal', 'cuerpo')
      ->where('id', "=", $id)
      ->first();

      $colaboradores = ColaboradorNoticia::select('id', 'nombre')
      ->where("colaborador_noticia.fk_id_noticia", "=", $id)
      ->join("colaborador", "colaborador_noticia.fk_id_colaborador", "=", "colaborador.id")
      ->get();

      if ($noticia) {
          $noticia->colaboradores = $colaboradores;
          $noticia->fecha_publicacion_dif = Carbon::parse($noticia->fecha_publicacion)->diffForHumans();
          $noticia->fecha_actualizacion_dif = Carbon::parse($noticia->fecha_actualizacion)->diffForHumans();
      }
      return Response($noticia, 200);
    } catch(\Exception $e) {
      return Response("Error", 500);
    }
  }

  /**
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function irNoticiaUsuario($id)
  {
      $noticia = Noticia::select('id')
      ->where('id', "=", $id)
      ->first();

      if ($noticia) {
          return view("noticia/noticia_usuario", ["id" => $id]);
      } else {
          return redirect('/');
      }
  }
}
