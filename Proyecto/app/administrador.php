<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\RecuperarPass;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

/**
 * @property string $id
 * @property string $correo
 * @property string $password
 * @property string $cedula
 * @property string $nombre_completo
 * @property string $telefono
 * @property string $estado
 * @property Noticia[] $noticias
 * @property Rol[] $roles
 */
class Administrador extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'administrador';

    /**
     * @var array
     */
    protected $fillable = ['correo', 'password', 'cedula', 'nombre_completo', 'telefono', 'estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function noticias()
    {
        return $this->hasMany('App\Noticia', 'fk_id_administrador', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Rol', 'rol_administrador', 'fk_id_administrador', 'fk_id_rol');
    }
    //Auth
    public function getAuthPassword()
    {
        return $this->password;
    }
    //Notificacion recuperacion contraseña
    //Email a utilizar en password_resets
    public function getEmailForPasswordReset()
    {
        return $this->correo;
    }
    //Email al cual se enviara la notificacion
    public function routeNotificationForMail($notification)
    {
        return $this->correo;
    }
    //Notificacion personalizada para la recuperacion de contraseñas
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new RecuperarPass($token));
    }

    //Roles
    public function authorizeRoles($roles)
    {
        abort_unless($this->hasAnyRole($roles), 401);
        return true;
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                 return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('nombre', $role)->first()) {
            return true;
        }
        return false;
    }
}
