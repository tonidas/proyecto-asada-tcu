<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $numero
 * @property string $fk_id_colaborador
 * @property Colaborador $colaborador
 */
class Telefono extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'telefono';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function colaborador()
    {
        return $this->belongsTo('App\Colaborador', 'fk_id_colaborador', 'id');
    }
}
