<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $fk_id_colaborador
 * @property string $url
 * @property Colaborador $colaborador
 */
class ImagenColab extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'imagen_colab';

    /**
     * @var array
     */
    protected $fillable = ['fk_id_colaborador', 'url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function colaborador()
    {
        return $this->belongsTo('App\Colaborador', 'fk_id_colaborador', 'id');
    }
}
