<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TablaRolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol')->insert([
            'nombre' => 'Superusuario'
        ]);
        DB::table('rol')->insert([
            'nombre' => 'Administrador'
        ]);
    }
}
