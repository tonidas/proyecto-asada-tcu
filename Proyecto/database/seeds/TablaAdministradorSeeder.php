<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TablaAdministradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('administrador')->insert([
            'correo' => 'admin1@gmail.com',
            'password' => Hash::make('0123456789'),
            'cedula' => '406740569',
            'nombre_completo' => 'Mata Moros Soila',
            'telefono' => '12345678',
            'estado' => 1
        ]);
        DB::table('administrador')->insert([
            'correo' => 'admin2@gmail.com',
            'password' => Hash::make('0123456789'),
            'cedula' => '406980457',
            'nombre_completo' => 'Mata Moros Juan',
            'telefono' => '12345667',
            'estado' => 1
        ]);
    }
}
