@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/colaborador/colaborador_usuario.css')}}">
@endsection

@section('titulo', 'Colaborador')

@section('contenido')
<script>
const id = "{{$id}}";
</script>
<div id="colaboradorUsuario"></div>
@endsection
