@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="css/colaborador/colaboradores_usuario.css">
@endsection

@section('titulo', 'Colaboradores')

@section('contenido')
<div id="colaboradoresUsuario"></div>
@endsection
