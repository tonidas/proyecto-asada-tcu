@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Recuperación de contraseña
        @endcomponent
    @endslot


{{-- Body --}}
<strong>{{ $data['nombre'] }}</strong>

Estás recibiendo este correo debido a que recibimos una solicitud para restablecer la contraseña de esta cuenta.

@component('mail::button', ['url' => $data['url'], 'color' => 'blue'])
Restablecer contraseña
@endcomponent

Si no fuiste tú quien hizo la solicitud, ignora este mensaje.

{{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent
