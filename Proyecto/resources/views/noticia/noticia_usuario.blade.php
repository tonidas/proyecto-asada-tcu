@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/noticia/noticia_usuario.css')}}">

<script type="text/javascript" src="{{asset('js/noticia/noticia_usuario.js')}}"></script>
@endsection

@section('titulo', '.')

@section('contenido')
    <script>
        const id = {{$id}};
    </script>
    <div id="noticiaUsuario"></div>
@endsection
