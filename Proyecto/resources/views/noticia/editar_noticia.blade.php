@extends('layouts.base')

@section('links')
<link rel="stylesheet" href="{{asset('libs/typeahead/Typeahead.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('libs/cropper/css/cropper.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/noticia/editar_noticia.css')}}">

<script type="text/javascript" src="{{asset('libs/cropper/js/cropper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('libs/tinymce/tinymce.min.js')}}"></script>
@endsection

@section('titulo', 'Editar noticia')

@section('header')
@parent
@endsection

@section('contenido')
<script>
const id = {{$id}};
</script>
<div id="editarNoticia"></div>
@endsection

@section('footer')
@endsection
