@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/administrador/editar_administrador.css')}}">

<script type="text/javascript" src="{{asset('libs/validator/validator.js')}}"></script>
@endsection

@section('titulo', 'Editar administrador')

@section('header')
@parent
@endsection

@section('contenido')
<script>
const id = {{$id}};
</script>
<div id="editarAdministrador"></div>
@endsection

@section('footer')
@endsection
