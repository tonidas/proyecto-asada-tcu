@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/administrador/crear_administrador.css')}}">

<script type="text/javascript" src="{{asset('libs/validator/validator.js')}}"></script>
@endsection

@section('titulo', 'Crear administrador')

@section('header')
@parent
@endsection

@section('contenido')
<div id="crearAdministrador"></div>
@endsection

@section('footer')
@endsection
