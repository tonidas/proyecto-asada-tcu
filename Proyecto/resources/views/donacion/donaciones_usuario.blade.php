@extends('layouts.base')

@section('links')
<link rel="stylesheet" type="text/css" href="{{asset('css/donacion/donaciones_usuario.css')}}">
@endsection

@section('titulo', 'Donaciones')

@section('contenido')
    <div id="donacionesUsuario">
    </div>
@endsection
