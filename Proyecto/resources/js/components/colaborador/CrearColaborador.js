import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Swal from 'sweetalert2'
import { Typeahead } from 'react-bootstrap-typeahead';
import { useDropzone } from 'react-dropzone';
import ImagesDragDropPreview from './ImagesDragDropPreview';
import FormColaborador from './FormColaborador';
import ListadosVTR from './ListadosVTR';
import validator from 'validator';

const axios = require('axios');
const imageType = require('image-type');

const crsf = $('meta[name=csrf-token]').attr("content");
const maximo = 5e+6;
const imgMax = 5;
const aceptado = 'image/jpeg, image/png';
const url = url_g;
const urlImgService = urlImgService_g;
const sleep = (milliseconds) => { return new Promise(resolve => setTimeout(resolve, milliseconds)) }
const scrollToElement = (element) => {
    $([document.documentElement, document.body]).animate({
            scrollTop: $(element).offset().top - $("nav").height()
    }, 0);
};

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

export default class CrearColaborador extends Component {

    constructor(props) {
        super(props)
        this.imagesDragDropPreview = React.createRef();

        this.state = {
            form: {
                cedula: "",
                nombre: "",
                direccion: "",
                descripcion: "",
                vision: "",
                mision: "",
            },
            formErrors: {
                cedula: "",
                nombre: "",
                direccion: "",
                descripcion: "",
                vision: "",
                mision: ""
            },
            listForm: {
                valor: "",
                telefono: "",
                red: ""
            },
            listFormErrors: {
                valor: "",
                telefono: "",
                red: ""
            },
            valores: [],
            telefonos: [],
            redes: [],
            imagenes: [],
            imagenesOpts: {
                selTodo: true,
                desTodo: true,
                eliminar: true,
            },
            opcionesDragDropPreview: {
                disabled: false,
                accept: aceptado,
                maxSize: maximo
            },
            valoresOpts: {
                selTodo: true,
                desTodo: true,
                eliminar: true,
            },
            telefonosOpts: {
                selTodo: true,
                desTodo: true,
                eliminar: true,
            },
            redesOpts: {
                selTodo: true,
                desTodo: true,
                eliminar: true,
            },
        }
        this.eliminarVTR = this.eliminarVTR.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.agregarValor = this.agregarValor.bind(this)
        this.agregarTelefono = this.agregarTelefono.bind(this)
        this.agregarRed = this.agregarRed.bind(this)
        this.procesarArchivos = this.procesarArchivos.bind(this);
        this.procesarImagen = this.procesarImagen.bind(this);
        this.setIImagenesOpts = this.setIImagenesOpts.bind(this);
        this.setEstadoAccionesDragDropImg = this.setEstadoAccionesDragDropImg.bind(this);
        this.setEstadoAcciones = this.setEstadoAcciones.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeList = this.handleChangeList.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    setIImagenesOpts(selTodo, desTodo, eliminar) {
        this.setState({
            imagenesOpts: {
                selTodo: selTodo,
                desTodo: desTodo,
                eliminar: eliminar,
            }
        });
    }

    handleChangeList(e) {
        const elemento = e.target;
        const nombre = e.target.name;
        let valor = e.target.value;
        let listFormErrors = this.state.listFormErrors;
        let inicial = false;

        if (!validator.isEmpty(valor.trim())) {

            switch (nombre) {
                case 'valor':
                    listFormErrors.valor = "";
                    break;
                case 'telefono':
                    valor = valor.replace(/-/g, "");

                    if (!validator.isNumeric(valor, { no_symbols: true })) {
                        listFormErrors.telefono = "Solo se aceptan números enteros positivos";

                    } else if (valor.length < $(elemento).attr("minLength") - 1) {
                        listFormErrors.telefono = "El tamaño mínimo es " + ($(elemento).attr("minLength") - 1);

                    } else {
                        listFormErrors.telefono = "";
                    }
                    break;
                case 'red':
                    if (!validator.isURL(e.target.value, { protocols: ['http', 'https'] })) {
                        listFormErrors.red = "La dirección ingresada es invalida";
                    } else {
                        listFormErrors.red = "";
                    }
                    break;
            }
        } else {
            listFormErrors[nombre] = "";
            inicial = true;
        }

        if (listFormErrors[nombre] != "") {
            $(elemento).removeClass("is-valid");
            $(elemento).addClass("is-invalid");
            $(elemento).closest("div").next(".invalid-feedback").html(listFormErrors[nombre]).show();
        } else {
            $(elemento).removeClass("is-invalid");

            if (!inicial) {
                $(elemento).addClass("is-valid");
            } else {
                $(elemento).removeClass("is-valid");
            }
            $(elemento).closest("div").next(".invalid-feedback").html("").hide();
        }
        this.setState({
            listForm: {
                ...this.state.listForm,
                [nombre]: valor
            },
            listFormErrors
        })
    }

    handleChange(e) {
        const elemento = e.target;
        const nombre = e.target.name;
        const valor = e.target.value;
        let formErrors = this.state.formErrors;

        switch (nombre) {
            case 'cedula':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.cedula = "Este espacio es requerido";

                } else if (!validator.isNumeric(valor, { no_symbols: true })) {
                    formErrors.cedula = "Solo se aceptan números enteros positivos";

                } else if (valor.length < $(elemento).attr("minLength")) {
                    formErrors.cedula = "El tamaño mínimo es 10";

                } else {
                    formErrors.cedula = "";
                }
                break;
            case 'nombre':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.nombre = "Este espacio es requerido";
                } else {
                    formErrors.nombre = "";
                }
                break;
            case 'direccion':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.direccion = "Este espacio es requerido";
                } else {
                    formErrors.direccion = "";
                }
                break;
            case 'descripcion':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.descripcion = "Este espacio es requerido";
                } else {
                    formErrors.descripcion = "";
                }
                break;
            case 'vision':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.vision = "Este espacio es requerido";
                } else {
                    formErrors.vision = "";
                }
                break;
            case 'mision':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.mision = "Este espacio es requerido";
                } else {
                    formErrors.mision = "";
                }
                break;
        }
        if (formErrors[nombre] != "") {
            $(elemento).removeClass("is-valid");
            $(elemento).addClass("is-invalid");
            $(elemento).parent().find(".invalid-feedback").html(formErrors[nombre]).show();
        } else {
            $(elemento).removeClass("is-invalid");
            $(elemento).addClass("is-valid");
            $(elemento).parent().find(".invalid-feedback").html("").hide();
        }
        this.setState({
            form: {
                ...this.state.form,
                [nombre]: valor
            },
            formErrors
        })
    }

    procesarArchivos() {
        let archivos = this.imagesDragDropPreview.current.state.archivos;
        let estado = {
            procesados: 0,
            completados: archivos.length > 0 ? archivos.filter((archivoT) => { return archivoT.progreso == 100; }).length : 0,
            erroneos: 0
        }
        return new Promise((resolve, reject) => {
            //Verificar que almenos exista un archivo
            if (archivos.length > 0) {
                //Procesar unicamente si hay pendientes
                if (estado.completados != archivos.length) {

                    const success = (archivo) => {
                        //Cambiar el archivo actual por el archivo procesado que contiene la URL generada para la imagen
                        archivos.forEach((archivoT) => {
                            if (archivoT.id == archivo.id) {
                                archivoT = archivo;
                                return;
                            }
                        });
                        //Actualizar estados
                        estado.completados++;
                        estado.procesados++;
                        //Si se han procesado todos los archivos, se actualizan en el componente hijo
                        if (estado.completados + estado.erroneos == archivos.length) {
                            this.imagesDragDropPreview.current.setState({
                                archivos: archivos
                            }, () => {
                                resolve(estado);
                            });
                        }
                    };

                    const failure = () => {
                        //Actualizar estados
                        estado.erroneos++;
                        estado.procesados++;

                        if (estado.completados + estado.erroneos == archivos.length) {
                            resolve(estado);
                        }
                    };

                    archivos.forEach(archivo => {
                        //Subir unicamente los que no esten completados
                        if (archivo.progreso != 100) {
                            this.procesarImagen(archivo, success, failure);
                        }
                    });
                } else {
                    resolve(estado);
                }
            } else {
                reject();
            }
        });
    }

    setEstadoAccionesDragDropImg(estado) {

        this.setState({
            //Desactivar opciones img
            opcionesDragDropPreview: {
                ...this.state.opcionesDragDropPreview,
                disabled: estado
            },
            imagenesOpts: {
                selTodo: estado,
                desTodo: estado,
                eliminar: true
            }
        });
    }

    setEstadoAcciones(estado) {
        //Desactivar formulario y botones de agregar
        $(".container .form-control, .btn").prop("disabled", estado);

        if (estado) {
            //Desactivar selecciones de tabs e imagenes
            $("#contValores a, #contTelefonos a, #contEnlaces a").removeClass("active");
            $("#prevs .prevFrente").removeClass('activo');
        }
        this.setState({
            //Desactivar acciones de listas
            valoresOpts: {
                selTodo: estado,
                desTodo: estado,
                eliminar: estado
            },
            telefonosOpts: {
                selTodo: estado,
                desTodo: estado,
                eliminar: estado
            },
            redesOpts: {
                selTodo: estado,
                desTodo: estado,
                eliminar: estado
            }
        });
        this.setEstadoAccionesDragDropImg(estado);
    }


    async handleSubmit(e) {
        e.preventDefault();
        const ref = this;
        this.setEstadoAcciones(true);
        //Establer carga
        $("#guardar").html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Guardando');
        //Verificar formulario
        Object.keys(this.state.form).forEach((key) => {
            let elemento = $("[name=" + key + "]")[0];
            this.handleChange({ target: elemento });
        });

        if ($("form .is-valid").length == Object.values(this.state.form).length) {

            if (!this.verificador()) {
                this.procesarArchivos()
                    .then((estado) => {

                        if (estado.erroneos > 0) {
                            this.setEstadoAcciones(false);
                            Toast.fire({
                                type: 'error',
                                title: 'No fue posible cargar algunas imágenes, intente de nuevo en un momento',
                                timer: 6000
                            });
                            this.setEstadoAcciones(false);
                            $("#guardar").html('Guardar');
                        } else {
                            //Obtener urls
                            const archivosT = this.imagesDragDropPreview.current.state.archivos;
                            let urls = [];
                            archivosT.forEach(archivoT => {
                                urls.push(archivoT.url);
                            });
                            //Enviar contenido al servidor
                            axios.post(url + 'colaborador_guardar',
                                {
                                    "form": this.state.form,
                                    "valores": this.state.valores,
                                    "telefonos": this.state.telefonos,
                                    "redes": this.state.redes,
                                    "imagenes": urls
                                })
                                .then(function(response) {

                                    if (response.status == 200) {
                                        window.location.href = url + "colaborador";
                                    }
                                })
                                .catch(function(e) {

                                    if (e.response.data.erroresForm != null) {
                                        const erroresForm = e.response.data.erroresForm;
                                        let formErrors = ref.state.formErrors;

                                        Object.keys(erroresForm).forEach((nombreInput) => {
                                            //Asignar el primer error de cada input
                                            formErrors[nombreInput] = erroresForm[nombreInput][0];
                                            const elemento = $("[name=" + nombreInput + "]");

                                            if (formErrors[nombreInput] != "") {
                                                elemento.removeClass("is-valid");
                                                elemento.addClass("is-invalid");
                                                elemento.parent().find(".invalid-feedback").html(formErrors[nombreInput]).show();
                                            } else {
                                                elemento.removeClass("is-invalid");
                                                elemento.addClass("is-valid");
                                                elemento.parent().find(".invalid-feedback").html("").hide();
                                            }
                                        });

                                        ref.setState({
                                            formErrors
                                        }, () => {
                                              scrollToElement($(".is-invalid").parent());
                                        })
                                    } else {
                                        Toast.fire({
                                            type: 'error',
                                            title: 'Error al crear el colaborador, intente de nuevo en un momento',
                                            timer: 6000
                                        });
                                    }
                                    ref.setEstadoAcciones(false);
                                    $("#guardar").html('Guardar');
                                });
                        }
                    })
                    .catch(() => {
                        Toast.fire({
                            type: 'error',
                            title: 'No se han agregado imágenes al colaborador',
                            timer: 6000
                        });
                        this.setEstadoAcciones(false);
                        $("#guardar").html('Guardar');
                    });
            } else {
                this.setEstadoAcciones(false);
                $("#guardar").html('Guardar');
            }
        } else {
            this.setEstadoAcciones(false);
            $("#guardar").html('Guardar');
              scrollToElement($(".is-invalid").parent());
        }
    }

    verificador() {
        let error = null;

        if (this.imagesDragDropPreview.current.state.archivos.length < 1) {
            error = 'No se han agregado imágenes al colaborador';
        }

        if (error) {
            Toast.fire({
                type: 'error',
                title: error,
                timer: 6000
            });
            return true;
        } else {
            return false;
        }
    }

    eliminarVTR(e) {
        let tipo;
        let tipoMensaje = "";
        let target = $(e.currentTarget);

        switch (target.parent().parent().data("key")) {
            case 0:
                tipo = 0;
                tipoMensaje = "los valores seleccionados";
                break;
            case 1:
                tipo = 1;
                tipoMensaje = "los teléfonos seleccionados";
                break;
            case 2:
                tipo = 2;
                tipoMensaje = "las redes sociales seleccionadas";
                break;
        }
        Swal.fire({
            title: 'Confirmación',
            text: "¿Estas seguro de eliminar " + tipoMensaje + "?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            preConfirm: (respuesta) => {

                if (respuesta) {
                    let tabs = [];
                    const tabsS = target.parent().parent().find(".list-group-item.active");

                    switch (tipo) {
                        case 0:
                            tabs = this.state.valores;
                            break;
                        case 1:
                            tabs = this.state.telefonos;
                            break;
                        case 2:
                            tabs = this.state.redes;
                            break;
                    }
                    //Remover imagenes seleccionadas
                    $.each(tabsS, function(indice, tabS) {
                        tabs = tabs.filter(tab => tab.id !== $(tabS).data("key"));
                    });

                    switch (tipo) {
                        case 0:
                            if (tabs.length > 0) {
                                this.setState({
                                    valoresOpts: {
                                        selTodo: false,
                                        desTodo: false,
                                        eliminar: true
                                    }
                                });
                            } else {
                                this.setState({
                                    valoresOpts: {
                                        selTodo: true,
                                        desTodo: true,
                                        eliminar: true
                                    }
                                });
                            }
                            this.setState({
                                valores: tabs
                            });
                            break;
                        case 1:
                            if (tabs.length > 0) {
                                this.setState({
                                    telefonosOpts: {
                                        selTodo: false,
                                        desTodo: false,
                                        eliminar: true
                                    }
                                });
                            } else {
                                this.setState({
                                    telefonosOpts: {
                                        selTodo: true,
                                        desTodo: true,
                                        eliminar: true
                                    }
                                });
                            }
                            this.setState({
                                telefonos: tabs,
                            });
                            break;
                        case 2:
                            if (tabs.length > 0) {
                                this.setState({
                                    redesOpts: {
                                        selTodo: false,
                                        desTodo: false,
                                        eliminar: true
                                    }
                                });
                            } else {
                                this.setState({
                                    redesOpts: {
                                        selTodo: true,
                                        desTodo: true,
                                        eliminar: true
                                    }
                                });
                            }
                            this.setState({
                                redes: tabs,
                            });
                            break;
                    }
                }
            }
        })
            .then((data) => {

                if (data.value == "Error") {
                    Toast.fire({
                        type: 'error',
                        title: 'Ha ocurrido un error, intente de nuevo en un momento',
                        timer: 6000
                    });
                } else if (data.value == null) {
                } else {
                    Toast.fire({
                        type: 'success',
                        title: 'Eliminado correctamente',
                    });
                }
            })
            .catch((e) => {
                Toast.fire({
                    type: 'error',
                    title: 'Ha ocurrido un error, intente de nuevo en un momento',
                    timer: 6000
                });
            });
    }

    generarIdsAleatorios(array, max, min) {
        let rand;
        let arrayT;
        array.forEach(item => {

            if (item.id == 0) {
                //Generar id aleatorio
                do {
                    rand = Math.floor(Math.random() * ((max - min) + 1)) + min;
                    arrayT = array.find((item, i) => {
                        return item.id == rand;
                    });
                } while (arrayT != null);
                //Asignar id generado
                item.id = rand;
            }
        });
    }

    agregarValor() {
        const max = 100;

        if (this.state.valores.length < max) {
            const valores = $("#contValores a");
            const valor = $("input[name=valor]");
            let error = "";

            valores.removeClass("active");
            this.setState({
                valoresOpts: {
                    selTodo: false,
                    desTodo: false,
                    eliminar: true
                }
            });

            if (!validator.isEmpty($(valor).val(), { ignore_whitespace: true })) {

                if (!valor.hasClass("is-invalid")) {
                    const valoresT = this.state.valores;
                    let res = true;

                    valoresT.forEach((valorT, i) => {

                        if (valorT.valor == $(valor).val().trim()) {
                            return res = false;
                        }
                    });

                    if (res) {
                        valoresT.push({ valor: $(valor).val(), id: 0 });
                        this.generarIdsAleatorios(valoresT, max, 1);
                        this.setState({
                            valores: valoresT,
                            listForm: {
                                ...this.state.listForm,
                                valor: ""
                            }
                        });
                        $(valor).removeClass("is-valid");
                        return;
                    } else {
                        error = "No es posible agregar valores repetidos";
                    }
                }
            } else {
                error = "No se ha ingresado ningún valor";
            }

            if (error != "") {
                $(valor).removeClass("is-valid");
                $(valor).addClass("is-invalid");
                $(valor).closest("div").next(".invalid-feedback").html(error).show();
            }
            valor.focus();
        } else {
            Toast.fire({
                type: 'error',
                title: 'No es posible agregar más valores',
                timer: 6000
            });
        }
    }

    agregarTelefono() {
        const max = 100;

        if (this.state.telefonos.length < max) {
            const telefonos = $("#contTelefonos a");
            const telefono = $("input[name=telefono]");
            let error = "";

            telefonos.removeClass("active");
            this.setState({
                telefonosOpts: {
                    selTodo: false,
                    desTodo: false,
                    eliminar: true
                }
            });
            let valor = $(telefono).val().trim();
            valor = valor.replace(/-/g, "");

            if (!validator.isEmpty(valor)) {

                if (!telefono.hasClass("is-invalid")) {
                    const telefonosT = this.state.telefonos;
                    let res = true;

                    telefonosT.forEach((telefonoT, i) => {

                        if (telefonoT.numero == valor) {
                            return res = false;
                        }
                    });

                    if (res) {
                        let telefonosT = this.state.telefonos;
                        telefonosT.push({ numero: valor, id: 0 });
                        this.generarIdsAleatorios(telefonosT, max, 1);
                        this.setState({
                            telefonos: telefonosT,
                            listForm: {
                                ...this.state.listForm,
                                telefono: ""
                            }
                        });
                        $(telefono).removeClass("is-valid");
                        return;
                    } else {
                        error = "No es posible agregar teléfonos repetidos";
                    }
                }
            } else {
                error = "No se ha ingresado ningún teléfono";
            }

            if (error != "") {
                $(telefono).removeClass("is-valid");
                $(telefono).addClass("is-invalid");
                $(telefono).closest("div").next(".invalid-feedback").html(error).show();
            }
            telefono.focus();
        } else {
            Toast.fire({
                type: 'error',
                title: 'No es posible agregar más teléfonos',
                timer: 6000
            });
        }
    }

    agregarRed() {
        const max = 100;

        if (this.state.redes.length < max) {
            const redes = $("#contEnlaces a");
            const red = $("input[name=red]");
            let error = "";

            redes.removeClass("active");
            this.setState({
                redesOpts: {
                    selTodo: false,
                    desTodo: false,
                    eliminar: true
                }
            });

            if (!validator.isEmpty($(red).val(), { ignore_whitespace: true })) {

                if (!red.hasClass("is-invalid")) {
                    const redesT = this.state.redes;
                    let res = true;

                    redesT.forEach((redT, i) => {

                        if (redT.enlace == $(red).val().trim()) {
                            return res = false;
                        }
                    });

                    if (res) {
                        let redesT = this.state.redes;
                        redesT.push({ enlace: $(red).val(), id: 0 });
                        this.generarIdsAleatorios(redesT, max, 1);
                        this.setState({
                            redes: redesT,
                            listForm: {
                                ...this.state.listForm,
                                red: ""
                            }
                        });
                        $(red).removeClass("is-valid");
                        return;
                    } else {
                        error = "No es posible agregar redes sociales repetidas";
                    }
                }
            } else {
                error = "No se ha ingresado ningún enlace";
            }

            if (error != "") {
                $(red).removeClass("is-valid");
                $(red).addClass("is-invalid");
                $(red).closest("div").next(".invalid-feedback").html(error).show();
            }
            red.focus();
        } else {
            Toast.fire({
                type: 'error',
                title: 'No es posible agregar más redes sociales',
                timer: 6000
            });
        }
    }

    handleClick(e) {
        //Imagenes
        if (e.currentTarget.id == "selTodoImg") {

            if ($("#prevs").find(".prev").length > 0) {
                $("#prevs").find(".prevFrente").addClass("activo");
                $("#eliminarImg").prop("disabled", false);
                this.setState({
                    imagenesOpts: {
                        eliminar: false
                    }
                });
            }
        }

        if (e.currentTarget.id == "desTodoImg") {
            $("#prevs").find(".prevFrente.activo").removeClass("activo");
            this.setState({
                imagenesOpts: {
                    eliminar: true
                }
            });
        }

        if (e.currentTarget.id == "eliminarImg") {
            const prevs = $("#prevs").find(".prev .activo").parent();
            let archivos = this.imagesDragDropPreview.current.state.archivos;
            //Remover imagenes seleccionadas
            $.each(prevs, function(indice, prev) {
                archivos = archivos.filter(archivo => {
                    if (archivo.id !== $(prev).data("key")) {
                        return true;
                    } else {
                        //Eliminar objetos url innecesarios
                        URL.revokeObjectURL(archivo.imagen.preview);
                    }
                });
            });
            //Aplicar cambios
            this.imagesDragDropPreview.current.setState({
                archivos: archivos
            });
            //Eliminar el tooltip de eliminar
            $(e.currentTarget).tooltip('dispose');
            //Actualizar las imagenes y el estado del eliminar
            this.setState({
                imagenesOpts: {
                    eliminar: true
                }
            });
            //Verificar si es necesario ocultar el panel de imagenes
            if (archivos.length == 0) {
                $("#contPrevImg").hide();
            }
        }

        if ($(e.currentTarget).attr("name") == "prev") {
            $(e.currentTarget).find(".prevFrente").toggleClass('activo');

            if ($("#prevs").find(".prev .activo").length > 0) {
                this.setState({
                    imagenesOpts: {
                        eliminar: false
                    }
                });
            } else {
                this.setState({
                    imagenesOpts: {
                        eliminar: true
                    }
                });
            }
        }
        //Valores, telefonos y redes
        if ($(e.currentTarget).attr("name") == "tabs") {
            $(e.currentTarget).toggleClass('active');
            const tipo = $(e.currentTarget).parents("div:eq(2)").data("key");
            let estado;

            if ($(e.currentTarget).parent().find(".active").length > 0) {
                estado = false;
            } else {
                estado = true;
            }
            switch (tipo) {
                case 0:
                    this.setState({
                        valoresOpts: {
                            eliminar: estado
                        }
                    });
                    break;
                case 1:
                    this.setState({
                        telefonosOpts: {
                            eliminar: estado
                        }
                    });
                    break;
                case 2:
                    this.setState({
                        redesOpts: {
                            eliminar: estado
                        }
                    });
                    break;
            }
        }

        if ($(e.currentTarget).attr("name") == "selTodoTabs") {

            if ($(e.currentTarget).parent().parent().find(".list-group-item").length > 0) {
                $(e.currentTarget).parent().parent().find(".list-group-item").addClass("active");
                const tipo = $(e.currentTarget).parent().parent().data("key");

                switch (tipo) {
                    case 0:
                        this.setState({
                            valoresOpts: {
                                eliminar: false
                            }
                        });
                        break;
                    case 1:
                        this.setState({
                            telefonosOpts: {
                                eliminar: false
                            }
                        });
                        break;
                    case 2:
                        this.setState({
                            redesOpts: {
                                eliminar: false
                            }
                        });
                        break;
                }
            }
        }

        if ($(e.currentTarget).attr("name") == "desTodoTabs") {
            $(e.currentTarget).parent().parent().find(".list-group-item").removeClass("active");
            const tipo = $(e.currentTarget).parent().parent().data("key");

            switch (tipo) {
                case 0:
                    this.setState({
                        valoresOpts: {
                            eliminar: true
                        }
                    });
                    break;
                case 1:
                    this.setState({
                        telefonosOpts: {
                            eliminar: true
                        }
                    });
                    break;
                case 2:
                    this.setState({
                        redesOpts: {
                            eliminar: true
                        }
                    });
                    break;
            }
        }
    }

    procesarImagen(archivo, success, failure) {
        const ref = this;

        const setProgreso = (porcActual) => {
            let archivos = ref.imagesDragDropPreview.current.state.archivos;
            archivos.forEach(a => {
                if (a.id === archivo.id) {
                    a.progreso = porcActual;
                }
            });
            ref.imagesDragDropPreview.current.setState({
                archivos: archivos
            });
        }

        try {
            const reader = new FileReader();
            reader.readAsArrayBuffer(archivo.imagen);

            reader.onload = function() {
                const arrayBuffer = reader.result;
                const xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', urlImgService + 'procesar.php');
                xhr.onload = function() {
                    let json;

                    if (xhr.status != 200) {
                        //Error HTTP
                        setProgreso(0);
                        failure(xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location != 'string') {
                        //Error en el formato del JSON
                        setProgreso(0);
                        failure(xhr.responseText);
                        return;
                    }
                    archivo.url = json.location;
                    success(archivo);
                };

                xhr.upload.onprogress = function(e) {
                    const porcActual = ((e.loaded / e.total) * 100).toFixed(0);
                    setProgreso(porcActual);
                }

                xhr.onerror = function(e) {
                    setProgreso(0);
                    failure();
                }
                const formData = new FormData();
                formData.append('file', archivo.imagen, "imagen." + imageType(arrayBuffer).ext);
                xhr.send(formData);
            };
        } catch (e) {
            failure();
        }
    }

    render() {
        return (
            <div className="container mt-5 mb-5">
                <h3 className="color-1">Crear colaborador</h3>
                <hr></hr>
                <FormColaborador
                    form={this.state.form}
                    onChange={this.handleChange}
                />
                <ListadosVTR
                    onChangeList={this.handleChangeList}
                    onClick={this.handleClick}
                    valoresOpts={this.state.valoresOpts}
                    telefonosOpts={this.state.telefonosOpts}
                    redesOpts={this.state.redesOpts}
                    listForm={this.state.listForm}
                    eliminarVTR={this.eliminarVTR}
                    agregarValor={this.agregarValor}
                    agregarTelefono={this.agregarTelefono}
                    agregarRed={this.agregarRed}
                    valores={this.state.valores}
                    telefonos={this.state.telefonos}
                    redes={this.state.redes}
                />
                <div className="row">
                    <div className="form-group col-xl-12 col-lg-12 col-ms-12">
                        <label className="font-weight-bold">Imágenes</label>
                        <small className="mb-2 form-text text-muted">La cantidad máxima de imágenes es {imgMax}</small>
                        <small className="mb-2 form-text text-muted">El tamaño máximo permitido por imagen es {maximo / 1e+6}MB</small>
                        <small className="mb-2 form-text text-muted">Formatos permitidos {this.state.opcionesDragDropPreview.accept}</small>
                        <ImagesDragDropPreview
                            opciones={this.state.opcionesDragDropPreview}
                            ref={this.imagesDragDropPreview}
                            onClick={this.handleClick}
                            imagenesOpts={this.state.imagenesOpts}
                            setImagenesOpts={this.setIImagenesOpts}
                            setEstadoAccionesDragDropImg={this.setEstadoAccionesDragDropImg}
                            imgMax={imgMax}
                        />
                    </div>
                </div>
                <button id="guardar" type="submit" className="btn btn-success" onClick={this.handleSubmit}>Guardar</button>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('crearColaborador')) {
        ReactDOM.render(<CrearColaborador />, document.getElementById('crearColaborador'));
        $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        $("input, textarea").on("drop", function() { return false });
    }
});
