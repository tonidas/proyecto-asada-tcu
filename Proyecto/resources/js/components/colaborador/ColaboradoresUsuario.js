import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Pagination from "react-js-pagination";

const axios = require('axios');
const url = url_g;

export default class ColaboradoresUsuario extends Component {

    constructor(props) {
        super(props)
        this.state = {
            colaboradores: [],
            activePage: 1,
            itemsCountPerPage: 12,
            totalItemsCount: 1,
            pageRangeDisplayed: 3
        }
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    async componentDidMount() {

        try {
            const res = await axios.get(url + 'obtener_colaboradores_usuario?paginaActual=' + this.state.activePage + '&totalPagina=' + this.state.itemsCountPerPage);

            if (res.data.colaboradores.length == 0) {
                $("footer").animate({ opacity: 1 });
            }
            this.setState({
                colaboradores: res.data.colaboradores,
                itemsCountPerPage: res.data.totalPagina,
                totalItemsCount: res.data.total,
                activePage: this.state.activePage
            });
        } catch (e) {
        }
    }

    async handlePageChange(paginaActual) {

        try {
            const res = await axios.get(url + 'obtener_colaboradores_usuario?paginaActual=' + paginaActual + '&totalPagina=' + this.state.itemsCountPerPage);
            this.setState({
                colaboradores: res.data.colaboradores,
                itemsCountPerPage: res.data.totalPagina,
                totalItemsCount: res.data.total,
                activePage: paginaActual
            });
            $(window).scrollTop(0);
        } catch (e) {
        }
    }

    plantillaColaborador(colaborador, i) {

        if (this.state.colaboradores.length > 0) {

            if (i == this.state.colaboradores.length - 1) {
                $(".encabezado, .colaboradores").css({"display": "block"}).animate({"opacity": "1"});
                //Mostrar paginacion si hay mas elementos de los permitidos por pagina
                if (this.state.totalItemsCount > this.state.itemsCountPerPage) {
                    $(".paginacion").css({ "opacity": "0", "display": "flex" }).animate({ opacity: 1 });
                }
                $("footer").animate({ opacity: 1 });
            }
        }
        const $img = colaborador.imgColaborador[0].url.replace("/img_o/", "/img_sm/");
        //0 width 1 height
        if (colaborador.imgColaborador[0].dimensiones[0] > colaborador.imgColaborador[0].dimensiones[1]) {
            //Horizontal
            return (
                <div className="colaborador col-sm-12 col-md-6 col-lg-6 col-xl-6" key={colaborador.id}>
                    <a className="colaborador-h card" href={url + "colaborador_u/" + colaborador.id}>
                        <div className="card-body">
                            <h5 className="card-title titulo">{colaborador.nombre}</h5>
                            <p className="card-text descripcion">{colaborador.descripcion}</p>
                        </div>
                        <div className="imagen">
                            <img src={$img} className="card-img-top" alt="..." onLoad={(e) => this.handleImageLoad(e)} />
                            <div className="spinner-img-cont">
                                <div className="spinner-border text-primary" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            );
        } else {
            //Vertical
            return (
                <div className="colaborador col-sm-12 col-md-6 col-lg-6 col-xl-6" key={colaborador.id}>
                    <a className="colaborador-v card" href={url + "colaborador_u/" + colaborador.id}>
                        <div className="row">
                            <div className="col-12">
                                <div className="card-body">
                                    <h5 className="card-title titulo">{colaborador.nombre}</h5>
                                </div>
                            </div>
                        </div>
                        <div className="row no-gutters">
                            <div className="col-md-6">
                                <div className="card-body">
                                    <p className="card-text descripcion">{colaborador.descripcion}</p>
                                </div>
                            </div>
                            <div className="col-md-6 imagen">
                                <img src={$img} className="card-img" alt="..." onLoad={(e) => this.handleImageLoad(e)} />
                                <div className="spinner-img-cont">
                                    <div className="spinner-border text-primary" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            );
        }
    }

    handleImageLoad(e) {
        $(e.target).next().remove();
        $(e.target).css("visibility", "visible");
        $(e.target).animate({opacity: 1}, "fast");
    }

    render() {
        return (
            <div className="container mb-5 mt-5">
                <div className="spinner-page-cont">
                    <div className="spinner-border text-primary" role="status">
                        <span className="sr-only"></span>
                    </div>
                </div>
                <div className="mb-5">
                    <h2 className="encabezado display-4 color-2 text-center display-none opacity-0">Colaboradores</h2>
                </div>
                <div className="colaboradores card-columns mb-5 display-none opacity-0" >
                    {this.state.colaboradores.map((colaborador, i) => { return this.plantillaColaborador(colaborador, i) }
                    )}
                </div>
                <div className="paginacion justify-content-center display-none">
                    <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemsCountPerPage}
                        totalItemsCount={this.state.totalItemsCount}
                        pageRangeDisplayed={this.state.pageRangeDisplayed}
                        onChange={this.handlePageChange}
                        itemClass='page-item'
                        linkClass='page-link'
                    />
                </div>
            </div>
        )
    }
}

$(document).ready(function() {

    if (document.getElementById('colaboradoresUsuario')) {
        ReactDOM.render(<ColaboradoresUsuario />, document.getElementById('colaboradoresUsuario'));
        //Observer
        const observer = new MutationObserver((mutationList) => {
            mutationList.forEach((mutation) => {

                if (mutation.addedNodes.length) {
                  $(mutation.addedNodes[0]).find(".card").animate({ "opacity": "1" }, 500);
                  $(".spinner-page-cont").css("visibility", "hidden");
                }
            })
        });
        const colaboradores = document.querySelector(".colaboradores") // elemento que queremos observar
        // Opcions para el observer
        const observerOptions = {
            attributes: false,
            childList: true,
            subtree: false,
            characterData: false,
            attributeOldValue: false,
            characterDataOldValue: false
        };
        observer.observe(colaboradores, observerOptions);
    }
});
