import React from 'react';
import InputMask from 'react-input-mask';

const ListadosVTR = (props) => (
    <div className="row">
        <div id="valores" data-key="0" className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label className="font-weight-bold">Valores</label>
            <div className="mb-1">
                <button name="selTodoTabs" onClick={props.onClick} data-toggle="tooltip" data-placement="top" title="Seleccionar todo" className="selTodo cont-icono btn btn-outline-info mr-2" disabled={props.valoresOpts.selTodo}><i className="far fa-check-square"></i></button>
                <button name="desTodoTabs" onClick={props.onClick} data-toggle="tooltip" data-placement="top" title="Cancelar selección" className="desTodo cont-icono btn btn-outline-info mr-2" disabled={props.valoresOpts.desTodo}><i className="far fa-square"></i></button>
                <button onClick={props.eliminarVTR} data-toggle="tooltip" data-placement="top" title="Eliminar selección" className="eliminar cont-icono btn btn-outline-danger" disabled={props.valoresOpts.eliminar}><i className="far fa-trash-alt"></i></button>
            </div>
            <div className="input-group">
                <input name="valor" onChange={props.onChangeList} value={props.listForm.valor} data-btn="#agregarValor" type="text" maxLength="100" placeholder="Escriba el valor" className="form-control" />
                <div className="input-group-append">
                    <input id="agregarValor" type="button" className="btn btn-primary" onClick={props.agregarValor} value="Agregar" />
                </div>
            </div>
            <div className="invalid-feedback"></div>
            <div className="mt-1">
                <div id="contValores" className="list-group cont-items border rounded border-gray p-1" role="tablist">
                    {props.valores.map((valor, i) => {
                        return <a data-key={valor.id} key={valor.id} name="tabs" onClick={(e) => { !props.valoresOpts.selTodo ? props.onClick(e) : undefined }} className="cursor-pointer list-group-item list-group-item-action p-1" id="list-home-list" role="tab">{valor.valor}</a>
                    })}
                </div>
            </div>
        </div>

        <div id="telefonos" data-key="1" className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label className="font-weight-bold">Teléfonos</label>
            <div className="mb-1">
                <button name="selTodoTabs" onClick={props.onClick} data-toggle="tooltip" data-placement="top" title="Seleccionar todo" className="selTodo cont-icono btn btn-outline-info mr-2" disabled={props.telefonosOpts.selTodo}><i className="far fa-check-square"></i></button>
                <button name="desTodoTabs" onClick={props.onClick} data-toggle="tooltip" data-placement="top" title="Cancelar selección" className="desTodo cont-icono btn btn-outline-info mr-2" disabled={props.telefonosOpts.desTodo}><i className="far fa-square"></i></button>
                <button onClick={props.eliminarVTR} onClick={props.eliminarVTR} data-toggle="tooltip" data-placement="top" title="Eliminar selección" className="eliminar cont-icono btn btn-outline-danger" disabled={props.telefonosOpts.eliminar}><i className="far fa-trash-alt"></i></button>
            </div>
            <div className="input-group">
                <InputMask
                    className="form-control"
                    name="telefono"
                    mask="9999-9999"
                    value={props.listForm.telefono}
                    onChange={props.onChangeList}
                    data-btn="#agregarTelefono"
                    placeholder="Escriba el teléfono"
                    minLength="9"
                    maskChar=""
                    alwaysShowMask={false}
                />
                <div className="input-group-append">
                    <input id="agregarTelefono" type="button" className="btn btn-primary" onClick={props.agregarTelefono} value="Agregar" />
                </div>
            </div>
            <div className="invalid-feedback"></div>
            <div className="mt-1">
                <div id="contTelefonos" className="list-group cont-items border rounded border-gray p-1" role="tablist">
                    {props.telefonos.map((telefono, i) => {
                        return <a data-key={telefono.id} key={telefono.id} name="tabs" onClick={(e) => { !props.telefonosOpts.selTodo ? props.onClick(e) : undefined }} className="cursor-pointer list-group-item list-group-item-action p-1" id="list-home-list" role="tab">{telefono.numero.slice(0, 4) + "-" + telefono.numero.slice(4)}</a>
                    })}
                </div>
            </div>
        </div>

        <div id="redes" data-key="2" className="form-group col-xl-6 col-lg-6 col-ms-12">
            <label className="font-weight-bold">Redes sociales</label>
            <div className="mb-1">
                <button name="selTodoTabs" onClick={props.onClick} data-toggle="tooltip" data-placement="top" title="Seleccionar todo" className="selTodo cont-icono btn btn-outline-info mr-2" disabled={props.redesOpts.selTodo}><i className="far fa-check-square"></i></button>
                <button name="desTodoTabs" onClick={props.onClick} data-toggle="tooltip" data-placement="top" title="Cancelar selección" className="desTodo cont-icono btn btn-outline-info mr-2" disabled={props.redesOpts.desTodo}><i className="far fa-square"></i></button>
                <button onClick={props.eliminarVTR} data-toggle="tooltip" data-placement="top" title="Eliminar selección" className="eliminar cont-icono btn btn-outline-danger" disabled={props.redesOpts.eliminar}><i className="far fa-trash-alt"></i></button>
            </div>
            <div className="input-group">
                <input name="red" onChange={props.onChangeList} value={props.listForm.red} data-btn="#agregarEnlace" placeholder="Ingrese el enlace" maxLength="200" className="form-control" />
                <div className="input-group-append">
                    <input id="agregarEnlace" type="button" className="btn btn-primary" onClick={props.agregarRed} value="Agregar" />
                </div>
            </div>
            <div className="invalid-feedback"></div>
            <div className="mt-1">
                <div id="contEnlaces" className="list-group cont-items border rounded border-gray p-1" role="tablist">
                    {props.redes.map((red, i) => {
                        return <a data-key={red.id} key={red.id} name="tabs" onClick={(e) => { !props.redesOpts.selTodo ? props.onClick(e) : undefined }} className="cursor-pointer list-group-item list-group-item-action p-1" id="list-home-list" role="tab">{red.enlace}</a>
                    })}
                </div>
            </div>
        </div>
    </div>
)
export default ListadosVTR
