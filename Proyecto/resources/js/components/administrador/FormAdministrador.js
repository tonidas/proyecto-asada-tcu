import React from 'react';
import InputMask from 'react-input-mask';

const FormAdministrador = (props) => (
    <form>
        <div className="form-group">
            <label htmlFor="cedula" className="font-weight-bold">Cédula</label>
            <InputMask
                className="form-control"
                name="cedula"
                mask="9-9999-9999"
                value={props.form.cedula}
                onChange={props.onChange}
                minLength="11"
                maskChar=""
                alwaysShowMask={false}
            />
            <div className="invalid-feedback"></div>
            <small id="ayuda-cedula" className="form-text text-muted">Nueve dígitos, sin cero al inicio ni guiones</small>
        </div>
        <div className="form-group">
            <label htmlFor="nombreCompleto" className="font-weight-bold">Nombre completo</label>
            <input type="text" name="nombreCompleto" className="form-control" maxLength="100" value={props.form.nombreCompleto} onChange={props.onChange} />
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group">
            <label htmlFor="correo" className="font-weight-bold">Correo electrónico</label>
            <input type="text" autoComplete="email"  name="correo" className="form-control" maxLength="100" value={props.form.correo} onChange={props.onChange} />
            <div className="invalid-feedback"></div>
        </div>
        <div className="form-group">
            <label htmlFor="telefono" className="font-weight-bold">Teléfono</label>
            <InputMask
                className="form-control"
                name="telefono"
                mask="9999-9999"
                value={props.form.telefono}
                onChange={props.onChange}
                minLength="9"
                maskChar=""
                alwaysShowMask={false}
                autoComplete="off"
            />
            <div className="invalid-feedback"></div>
        </div>
        {props.edicion ? (
            <div>
                <div>
                    <label className="font-weight-bold">Cambiar contraseña</label>
                    <small className="mb-2 form-text text-muted">Opcional</small>
                    <div className="form-group">
                        <label htmlFor="contrasena">Contraseña</label>
                        <input type="password" autoComplete="new-password" name="contrasena" className="form-control" minLength="10" maxLength="20" value={props.form.contrasena} onChange={props.onChange} />
                        <div className="invalid-feedback"></div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="confirmarContrasena">Confirmar contraseña</label>
                        <input type="password" autoComplete="new-password" name="confirmarContrasena" className="form-control" minLength="10" maxLength="20" value={props.form.confirmarContrasena} onChange={props.onChange} />
                        <div className="invalid-feedback"></div>
                    </div>
                </div>
            </div>
        ) : (
                <div>
                    <div className="form-group">
                        <label htmlFor="contrasena" className="font-weight-bold">Contraseña</label>
                        <input type="password" autoComplete="new-password" name="contrasena" className="form-control" minLength="10" maxLength="20" value={props.form.contrasena} onChange={props.onChange} />
                        <div className="invalid-feedback"></div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="confirmarContrasena" className="font-weight-bold">Confirmar contraseña</label>
                        <input type="password" autoComplete="new-password" name="confirmarContrasena" className="form-control" minLength="10" maxLength="20" value={props.form.confirmarContrasena} onChange={props.onChange} />
                        <div className="invalid-feedback"></div>
                    </div>
                </div>
            )
        }
        {props.edicion ?
          <button id="actualizar" type="submit" className="btn btn-success" onClick={props.onSubmit}>Actualizar</button>
        :
          <button id="guardar" type="submit" className="btn btn-success" onClick={props.onSubmit}>Guardar</button>
        }
    </form>
)
export default FormAdministrador
