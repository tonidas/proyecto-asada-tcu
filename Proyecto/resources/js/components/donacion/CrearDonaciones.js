import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Swal from 'sweetalert2'
import FormDonacion from './FormDonacion';
import validator from 'validator';

const axios = require('axios');
const crsf = $('meta[name=csrf-token]').attr("content");
const url = url_g;
const scrollToElement = (element) => {
    $([document.documentElement, document.body]).animate({
            scrollTop: $(element).offset().top - $("nav").height()
    }, 0);
};

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

export default class CrearDonaciones extends Component {

    constructor(props) {
        super(props)
        this.imagesDragDropPreview = React.createRef();

        this.state = {
            form: {
                numero_cuenta: "",
                nombre_banco: "",
                nombre_propietario: "",
                moneda: ""
            },
            formErrors: {
                numero_cuenta: "",
                nombre_banco: "",
                nombre_propietario: "",
                moneda: ""
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const elemento = e.target;
        const nombre = e.target.name;
        let valor = e.target.value;
        let formErrors = this.state.formErrors;
        switch (nombre) {
            case 'numero_cuenta':
                valor = valor.replace(/-/g, "");

                if (validator.isEmpty(valor.trim())) {
                    formErrors.numero_cuenta = "Este espacio es requerido";

                } else if (!validator.isNumeric(valor, { no_symbols: true })) {
                    formErrors.numero_cuenta = "Solo se aceptan números enteros positivos";

                } else if (valor.length < $(elemento).attr("minLength") - 1) {
                    formErrors.numero_cuenta = "El tamaño mínimo es " + ($(elemento).attr("minLength") - 1);

                } else {
                    formErrors.numero_cuenta = "";
                }
                break;
            case 'nombre_propietario':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.nombre_propietario = "Este espacio es requerido";

                } else if (!valor.trim().match(/^[A-Za-z\sáéíóúÁÉÍÓÚ]+$/)) {
                    formErrors.nombre_propietario = "Solo se aceptan letras, acentos y espacios";

                } else {
                    formErrors.nombre_propietario = "";
                }
                break;
            case 'nombre_banco':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.nombre_banco = "Este espacio es requerido";

                } else if (!valor.trim().match(/^[A-Za-z\sáéíóúÁÉÍÓÚ]+$/)) {
                    formErrors.nombre_banco = "Solo se aceptan letras, acentos y espacios";

                } else {
                    formErrors.nombre_banco = "";
                }
                break;
            case 'moneda':
                if (validator.isEmpty(valor.trim())) {
                    formErrors.moneda = "Este espacio es requerido";

                } else if (!valor.trim().match(/^[A-Za-z\sáéíóúÁÉÍÓÚ]+$/)) {
                    formErrors.moneda = "Solo se aceptan letras, acentos y espacios";

                } else {
                    formErrors.moneda = "";
                }
                break;
        }

        if (formErrors[nombre] != "") {
            $(elemento).removeClass("is-valid");
            $(elemento).addClass("is-invalid");
            $(elemento).parent().find(".invalid-feedback").html(formErrors[nombre]).show();
        } else {
            $(elemento).removeClass("is-invalid");
            $(elemento).addClass("is-valid");
            $(elemento).parent().find(".invalid-feedback").html("").hide();
        }
        this.setState({
            form: {
                ...this.state.form,
                [nombre]: valor
            },
            formErrors
        })
    }

    async handleSubmit(e) {
        e.preventDefault();
        const ref = this;
        //Verificar formulario
        Object.keys(this.state.form).forEach((key) => {
            let elemento = $("[name=" + key + "]")[0];
            this.handleChange({ target: elemento });
        });

        if ($("form .is-valid").length == Object.values(this.state.form).length) {
            //Desactivar formulario y botones de agregar
            $(".container .form-control, .btn").prop("disabled", true);
            //Enviar contenido al servidor
            axios.post(url + 'donacion_guardar',
                {
                    "form": this.state.form
                })
                .then(function(response) {

                    if (response.status == 200) {
                        window.location.href = url + "donacion";
                    }
                })
                .catch(function(e) {

                    if (e.response.data.erroresForm != null) {
                        const erroresForm = e.response.data.erroresForm;
                        let formErrors = ref.state.formErrors;

                        Object.keys(erroresForm).forEach((nombreInput) => {
                            //Asignar el primer error de cada input
                            formErrors[nombreInput] = erroresForm[nombreInput][0];
                            const elemento = $("[name=" + nombreInput + "]");

                            if (formErrors[nombreInput] != "") {
                                elemento.removeClass("is-valid");
                                elemento.addClass("is-invalid");
                                elemento.parent().find(".invalid-feedback").html(formErrors[nombreInput]).show();
                            } else {
                                elemento.removeClass("is-invalid");
                                elemento.addClass("is-valid");
                                elemento.parent().find(".invalid-feedback").html("").hide();
                            }
                        });

                        ref.setState({
                            formErrors
                        }, () => {
                            scrollToElement($(".is-invalid").parent());
                        })
                    } else {
                        Toast.fire({
                            type: 'error',
                            title: 'Error al crear la cuenta donacion, intente de nuevo en un momento',
                            timer: 6000
                        });
                    }
                    //Activar formulario y botones de agregar
                    $(".container .form-control, .btn").prop("disabled", false);
                });
        } else {
            scrollToElement($(".is-invalid").parent());
        }
    }

    render() {
        return (
            <div className="container mt-5 mb-5">
                <h3 className="color-1">Crear donación</h3>
                <hr></hr>
                <FormDonacion
                    form={this.state.form}
                    onChange={this.handleChange}
                    onSubmit={this.handleSubmit}
                />
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('crearDonaciones')) {
        ReactDOM.render(<CrearDonaciones />, document.getElementById('crearDonaciones'));
        $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        $("input, textarea").on("drop", function() { return false });
    }
});
