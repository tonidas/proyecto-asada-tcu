import React from 'react';
import 'react-image-crop/dist/ReactCrop.css';
import ReactCrop from 'react-image-crop';

const VizualizadorCropper = (props) => (
    <div id="vizualizadorCropper" className="mb-3">
        <label>Área visible de la imagen</label>
        <div id="contenedorImg" className="border border-gray rounded">
            <ReactCrop
                src={props.src}
                crop={props.crop}
                onChange={props.onCropChange}
                onImageLoaded={props.onImageLoaded}
                onComplete={props.onComplete}
                imageStyle={{maxHeight: 700}}
                disabled={props.disabled}
            />
        </div>
    </div>
)
export default VizualizadorCropper
