import React from 'react';
import Cropper from 'cropperjs';

const ContenidoNoticia = (props) => (
    <div id="contenidoNoticia">
        <div className="row">
            <div className="form-group col-xl-12">
                <label className="font-weight-bold">Título</label>
                <input name="titulo" maxLength="200" type="text" className="form-control" onChange={props.onChange} value={props.form.titulo} />
                <div className="invalid-feedback"></div>
            </div>
            <div className="form-group col-xl-12">
                <label className="font-weight-bold">Descripción</label>
                <textarea className="form-control" onChange={props.onChange} value={props.form.descripcion} maxLength="500" name="descripcion" rows="4" resize="false" ></textarea>
                <div className="invalid-feedback"></div>
            </div>
            <div className="form-group col-xl-12">
                <label className="font-weight-bold">Cuerpo</label>
                <div className="tiny-container">
                    <textarea name="cuerpo" id="cuerpo"></textarea>
                </div>
            </div>
        </div>
    </div>
)
export default ContenidoNoticia
