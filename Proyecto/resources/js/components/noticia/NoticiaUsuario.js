import React, { Component } from 'react';
import ReactDOM from 'react-dom';
const url = url_g;
const axios = require('axios');

export default class NoticiaUsuario extends Component {

    constructor(props) {
        super(props)

        this.state = {
            noticia: [],
            noticiasRecientes: [],
            colaboradores: []
        }
    }

    async componentDidMount() {

        try {
            const res = await axios.get(url + 'obtener_noticia_usuario?id=' + id);
            this.setState({
                noticia: res.data,
                colaboradores: res.data.colaboradores
            });
        } catch (e) {

        }

        try {
            const result = await axios.get(url + 'obtener_noticias_recientes/' + 4);
            this.setState({
                noticiasRecientes: result.data
            });
        } catch (e) {

        }
        //Asignar titulo dinamico
        document.title = this.state.noticia.titulo;
        //Proceso de aparicion
        window.scrollTo(0, 0);
        $(".container").css({ "opacity": "0", "display": "block" }).animate({ opacity: 1 }, () => {
            $("footer").animate({ opacity: 1 });
        });
    }

    render() {
        return (
            <div className="container mt-5 mb-5 display-none opacity-0 ">
                <section id="noticia">
                    <article id="singlepost">
                        <div className="row">
                            <div id="contenido" className="col-12 col-sm-12 col-md-12 col-lg-8">
                                <div id="titulo">
                                    <h1>{this.state.noticia.titulo}</h1>
                                </div>
                                <div>
                                    <p className="text-muted"><em>Actualizada {this.state.noticia.fecha_actualizacion_dif}</em></p>
                                </div>
                                <div>
                                    <img className="mw-100" src={this.state.noticia.img_principal} alt="" />
                                </div>
                                <div id="descripcion" className="mt-3">
                                    {this.state.noticia.descripcion}
                                </div>
                                <div id="cuerpo">
                                    <div dangerouslySetInnerHTML={{ __html: this.state.noticia.cuerpo }} ></div>
                                </div>
                            </div>
                            <div id="panelD" className="col-12 col-sm-12 col-md-12 col-lg-4">
                                <h4>Noticias recientes</h4>
                                <div className="noticias-recientes row">
                                    {this.state.noticiasRecientes.map((noticiaReciente, i) =>
                                        <a key={noticiaReciente.id} href={url + "noticia_u/" + noticiaReciente.id} className="col-6">
                                            <div className="noticia-reciente" key={noticiaReciente.id}>
                                                <img src={noticiaReciente.img_principal.replace("/img_o/", "/img_sm/")} className="card-img-top" alt="..." />
                                                <div className="card-body">
                                                    <p className="card-title">{noticiaReciente.titulo}</p>
                                                </div>
                                            </div>
                                        </a>
                                    )}
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
                <hr className="hr-noticia" />
                <div className="mt-3" id="colaboradores" >
                    <h5>Colaboradores</h5>
                    <div>
                        {this.state.colaboradores.map((colaborador) =>
                            <a key={colaborador.id} href={url + "colaborador_u/" + colaborador.id} className="colaborador mr-1 mt-1 badge badge-pill badge-primary">{colaborador.nombre}</a>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

$(document).ready(function() {

    if (document.getElementById('noticiaUsuario')) {
        ReactDOM.render(<NoticiaUsuario />, document.getElementById('noticiaUsuario'));
    }
});
