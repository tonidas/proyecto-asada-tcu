/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
//require('./bootstrap');

//Variables globales de los componentes
window.url_g = 'http://127.0.0.1:8000/';
window.urlImgService_g = 'http://localhost/';

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/noticia/NoticiasAdmin');
require('./components/noticia/CrearNoticia');
require('./components/noticia/EditarNoticia');
require('./components/noticia/NoticiasUsuario');
require('./components/noticia/NoticiaUsuario');
require('./components/colaborador/ColaboradoresAdm');
require('./components/colaborador/CrearColaborador');
require('./components/colaborador/EditarColaborador');
require('./components/colaborador/ColaboradorUsuario');
require('./components/colaborador/ColaboradoresUsuario');
require('./components/administrador/AdministradoresSuperAdm');
require('./components/administrador/CrearAdministrador');
require('./components/administrador/EditarAdministrador');
require('./components/administracion/InicioSesion');
//Donaciones
require('./components/donacion/DonacionesAdmin');
require('./components/donacion/CrearDonaciones');
require('./components/donacion/EditarDonaciones');
require('./components/donacion/DonacionesUsuario');